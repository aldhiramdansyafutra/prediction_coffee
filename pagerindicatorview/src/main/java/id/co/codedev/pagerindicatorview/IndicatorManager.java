package id.co.codedev.pagerindicatorview;

import android.support.annotation.Nullable;

import id.co.codedev.pagerindicatorview.animation.AnimationManager;
import id.co.codedev.pagerindicatorview.animation.controller.ValueController;
import id.co.codedev.pagerindicatorview.animation.data.Value;
import id.co.codedev.pagerindicatorview.draw.DrawManager;
import id.co.codedev.pagerindicatorview.draw.data.Indicator;

public class IndicatorManager implements ValueController.UpdateListener {

    private DrawManager drawManager;
    private AnimationManager animationManager;
    private Listener listener;

    interface Listener {
        void onIndicatorUpdated();
    }

    IndicatorManager(@Nullable Listener listener) {
        this.listener = listener;
        this.drawManager = new DrawManager();
        this.animationManager = new AnimationManager(drawManager.indicator(), this);
    }

    public AnimationManager animate() {
        return animationManager;
    }

    public Indicator indicator() {
        return drawManager.indicator();
    }

    public DrawManager drawer() {
        return drawManager;
    }

    @Override
    public void onValueUpdated(@Nullable Value value) {
        drawManager.updateValue(value);
        if (listener != null) {
            listener.onIndicatorUpdated();
        }
    }
}
