package id.co.codedev.pagerindicatorview.draw.drawer.type;

import android.graphics.Paint;
import android.support.annotation.NonNull;

import id.co.codedev.pagerindicatorview.draw.data.Indicator;


class BaseDrawer {

    Paint paint;
    Indicator indicator;

    BaseDrawer(@NonNull Paint paint, @NonNull Indicator indicator) {
        this.paint = paint;
        this.indicator = indicator;
    }
}
