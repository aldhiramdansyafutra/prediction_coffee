package id.co.codedev.pagerindicatorview.animation.data.type;


import id.co.codedev.pagerindicatorview.animation.data.Value;

public class ThinWormAnimationValue extends WormAnimationValue implements Value {

    private int height;

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
