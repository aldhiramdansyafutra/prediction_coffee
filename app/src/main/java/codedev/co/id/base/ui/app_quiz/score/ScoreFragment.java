package codedev.co.id.base.ui.app_quiz.score;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.data.locale.PreferencesHelper;
import codedev.co.id.base.interfaces.DialogButtonListener;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.DialogFactory;

public class ScoreFragment extends BaseFragment {

    @BindView(R.id.tv_score_1)
    TextView tvScore1;
    @BindView(R.id.tv_score_2)
    TextView tvScore2;
    @BindView(R.id.tv_score_3)
    TextView tvScore3;

    @Inject
    PreferencesHelper mPreferenceHelper;

    View mVIewRoot;

    @Override
    protected void attachViewForPresenter() {

    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_app_quiz_score;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mVIewRoot = super.onCreateView(inflater, container, savedInstanceState);
        setView();
        return mVIewRoot;
    }

    @OnClick(R.id.btn_reset)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_reset:
                DialogFactory.createDialog(getActivity(), "Reset point kamu ?", "Dengan mereset point kamu maka kamu akan mengulang lagi level kuis yang sudah kamu selesaikan.",
                        "RESET", "BATAL", new DialogButtonListener() {
                            @Override
                            public void onPositiveDialogButtonListener() {
                                mPreferenceHelper.putString(PreferencesHelper.PREF_POINT_LEVEL_EASY, "0.0", "0.0");
                                mPreferenceHelper.putString(PreferencesHelper.PREF_POINT_LEVEL_MEDIUM, "0.0", "0.0");
                                mPreferenceHelper.putString(PreferencesHelper.PREF_POINT_LEVEL_HARD, "0.0", "0.0");
                                setView();
                            }

                            @Override
                            public void onNegativeDialogButtonListener() {

                            }
                        }).show();
                break;
        }
    }

    private void setView() {
        tvScore1.setText(mPreferenceHelper.getString(PreferencesHelper.PREF_POINT_LEVEL_EASY, "0.0"));
        tvScore2.setText(mPreferenceHelper.getString(PreferencesHelper.PREF_POINT_LEVEL_MEDIUM, "0.0"));
        tvScore3.setText(mPreferenceHelper.getString(PreferencesHelper.PREF_POINT_LEVEL_HARD, "0.0"));
    }
}
