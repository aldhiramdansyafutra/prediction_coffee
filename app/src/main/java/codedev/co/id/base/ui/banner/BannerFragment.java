package codedev.co.id.base.ui.banner;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.BindView;
import codedev.co.id.base.R;
import codedev.co.id.base.ui.base.BaseFragment;
import id.co.codedev.pagerindicatorview.PageIndicatorView;
import id.co.codedev.pagerindicatorview.animation.type.AnimationType;
import id.co.codedev.pagerindicatorview.draw.data.Orientation;
import id.co.codedev.pagerindicatorview.draw.data.RtlMode;
import id.co.codedev.wrappingviewpager.WrappingViewPager;

public class BannerFragment extends BaseFragment implements BannerMvpView {

    @BindView(R.id.pager_banner)
    WrappingViewPager mPagerBanner;
    @BindView(R.id.page_indicator_view)
    PageIndicatorView mPageIndicatorView;
    @BindView(R.id.container_banner)
    LinearLayout mContainerBanner;

    BannerPresenter mPresenter;
    BannerPagerAdapter mBannerPagerAdapter;

    View mViewRoot;

    @Override
    protected int getLayout() {
        return R.layout.fragment_banner;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new BannerPresenter(getActivity(), this);
        mBannerPagerAdapter = new BannerPagerAdapter(getFragmentManager());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mViewRoot = super.onCreateView(inflater, container, savedInstanceState);
        mPageIndicatorView.setAnimationType(AnimationType.WORM);
        mPageIndicatorView.setOrientation(Orientation.HORIZONTAL);
        mPageIndicatorView.setRtlMode(RtlMode.Off);
        return mViewRoot;
    }

    @Override
    protected void attachViewForPresenter() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.getBanners();
    }

    @Override
    public void onSucceedGetBannerList() {
        mBannerPagerAdapter.setBannerList(mPresenter.mBannerList);
        mPagerBanner.setAdapter(mBannerPagerAdapter);
        mPageIndicatorView.setViewPager(mPagerBanner);
    }
}
