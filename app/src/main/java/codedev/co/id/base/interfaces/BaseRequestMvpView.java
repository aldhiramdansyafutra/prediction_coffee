package codedev.co.id.base.interfaces;

public interface BaseRequestMvpView extends MvpView {

    void showProgressBarLoading();
    void onFailedRequest();
}
