package codedev.co.id.base.ui.app_prediction.conversion;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.inject.Inject;

import codedev.co.id.base.data.DataManager;
import codedev.co.id.base.data.model_prediction.Conversion;
import codedev.co.id.base.data.model_prediction.ConversionRequest;
import codedev.co.id.base.data.model_prediction.ConversionVariable;
import codedev.co.id.base.data.model_prediction.Product;
import codedev.co.id.base.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConversionPresenter extends BasePresenter<ConversionMvpView> {

    Conversion conversion;
    ConversionVariable mConversionVariable;

    public ArrayList<Product> mPredictionResult;

    int count;
    int valueX = 19;

    @Inject
    public ConversionPresenter(DataManager dataManager) {
        mDataManager = dataManager;
        mPredictionResult = new ArrayList<>();
    }

    public void getDataConversion(String productId, String year) {
        if (isViewAttached()) {
            mMvpView.showProgressBarLoading();
            ConversionRequest request = new ConversionRequest();
            request.productId = productId;
            request.year = year;
            mDataManager.getConversion(request).enqueue(new Callback<Conversion>() {
                @Override
                public void onResponse(Call<Conversion> call, Response<Conversion> response) {
                    conversion = response.body();
                    if (conversion != null) {
                        mConversionVariable = conversion.variable;
                        count = conversion.products.size();
                        mMvpView.onSucceed(conversion);
                    }
                }

                @Override
                public void onFailure(Call<Conversion> call, Throwable t) {
                    mMvpView.onFailedRequest();
                }
            });
        }
    }

    public void calculatePrediction() {
        for (Product product : conversion.products) {
            Product prediction = new Product();
            prediction.productId = product.productId;
            prediction.name = product.name;
            prediction.price = product.price;
            prediction.soldItem = product.soldItem + getValueLinearRegretion();
            prediction.month = product.month;
            prediction.year = product.year;
            mPredictionResult.add(prediction);
        }
        if (mPredictionResult.size() > 0) {
            mMvpView.onPredictionSucceed();
        }
    }

    private double getNilaiB() {
        return ((count * mConversionVariable.xY) - (mConversionVariable.x * mConversionVariable.y))
                / ((count * mConversionVariable.x2) - (mConversionVariable.x * mConversionVariable.x));
    }

    private double getNilaiA() {
        double a = (mConversionVariable.y - (getNilaiB() * mConversionVariable.x)) / count;

        String value = String.valueOf(a).replace("-", "");
        return Double.parseDouble(value);
    }

    public int getValueLinearRegretion() {
        BigDecimal decimal = new BigDecimal((getNilaiA() + ((getNilaiB() * valueX))) / count);
        return decimal.intValue();
    }
}
