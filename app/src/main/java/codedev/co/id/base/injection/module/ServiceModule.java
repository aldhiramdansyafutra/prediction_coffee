package codedev.co.id.base.injection.module;

import android.app.Service;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    Service mService;

    public ServiceModule(Service service) {
        mService = service;
    }

    @Provides
    Service provideService() {
        return mService;
    }
}
