package codedev.co.id.base.ui.app_prediction.kurs.form;

import javax.inject.Inject;

import codedev.co.id.base.data.DataManager;
import codedev.co.id.base.ui.base.BasePresenter;

public class KursFormPresenter extends BasePresenter<KursFormMvpView> {

    @Inject
    public KursFormPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }
}
