package codedev.co.id.base.ui.app_prediction.product.list;

import android.util.Log;

import javax.inject.Inject;

import codedev.co.id.base.data.DataManager;
import codedev.co.id.base.data.model_prediction.Product;
import codedev.co.id.base.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductPresenter extends BasePresenter<ProductMvpView> {

    @Inject
    public ProductPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    public void getProducts() {
        mDataManager.getAllProduct().enqueue(new Callback<Product.Result>() {
            @Override
            public void onResponse(Call<Product.Result> call, Response<Product.Result> response) {
                Product.Result result = response.body();
                if (result != null) mMvpView.onSucceedGetProducts(result.products);
            }

            @Override
            public void onFailure(Call<Product.Result> call, Throwable t) {
                Log.e("getProduct", "Failed");
            }
        });
    }
}
