package codedev.co.id.base.ui.app_prediction.product.list;

import java.util.ArrayList;

import codedev.co.id.base.data.model_prediction.Product;
import codedev.co.id.base.interfaces.MvpView;

public interface ProductMvpView extends MvpView {
    void onSucceedGetProducts(ArrayList<Product> results);
}
