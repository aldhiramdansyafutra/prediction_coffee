package codedev.co.id.base.ui.app_quiz.quiz;

import android.os.Bundle;

import codedev.co.id.base.interfaces.BaseView;

public interface QuizView extends BaseView {

    void onSucceedGetContentQuiz();
    void onNextQuestionPage(Bundle bundle);
    void onNextResultPage(Bundle bundle);

}
