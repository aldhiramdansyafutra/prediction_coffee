package codedev.co.id.base.injection.component;

import codedev.co.id.base.injection.PerFragment;
import codedev.co.id.base.injection.module.FragmentModule;
import codedev.co.id.base.ui.app_location.list.SchoolLocationFragment;
import codedev.co.id.base.ui.app_location.list.detail.SchoolListLocationDetailFragment;
import codedev.co.id.base.ui.app_location.maps.LocationMapsFragment;
import codedev.co.id.base.ui.app_prediction.conversion.ConversionFragment;
import codedev.co.id.base.ui.app_prediction.kurs.form.KursFormFragment;
import codedev.co.id.base.ui.app_prediction.kurs.list.KursListFragment;
import codedev.co.id.base.ui.app_prediction.product.list.ProductFragment;
import codedev.co.id.base.ui.app_prediction.product.form.ProductFormFragment;
import codedev.co.id.base.ui.app_quiz.home.HomeFragment;
import codedev.co.id.base.ui.app_quiz.quiz.QuizFragment;
import codedev.co.id.base.ui.app_quiz.quiz.QuizResultFragment;
import codedev.co.id.base.ui.app_quiz.score.ScoreFragment;
import codedev.co.id.base.ui.base.BaseFragment;
import dagger.Component;

/**
 * This component inject dependencies to all Fragments across the application
 */
@PerFragment
@Component(dependencies = {ApplicationComponent.class}, modules = FragmentModule.class)
public interface FragmentComponent {

    void inject(BaseFragment baseFragment);

    void inject(SchoolLocationFragment schoolLocationFragment);

    void inject(SchoolListLocationDetailFragment schoolListLocationDetailFragment);

    void inject(LocationMapsFragment locationMapsFragment);

    void inject(HomeFragment homeFragment);

    void inject(QuizFragment quizFragment);

    void inject(QuizResultFragment quizResultFragment);

    void inject(ScoreFragment scoreFragment);

    void inject(ProductFragment productFragment);

    void inject(ProductFormFragment productFormFragment);

    void inject(codedev.co.id.base.ui.app_prediction.home.HomeFragment homeFragment);

    void inject(ConversionFragment conversionFragment);

    void inject(KursListFragment kursListFragment);

    void inject(KursFormFragment kursFormFragment);
}