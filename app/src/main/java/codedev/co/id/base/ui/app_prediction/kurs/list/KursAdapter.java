package codedev.co.id.base.ui.app_prediction.kurs.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import codedev.co.id.base.R;
import codedev.co.id.base.data.model_prediction.Kurs;

public class KursAdapter extends RecyclerView.Adapter<KursAdapter.ProductViewHolder> {

    private ArrayList<Kurs> listProduct;
    private OnItemClickListener mOnItemClickListener;

    public KursAdapter() {
        listProduct = new ArrayList<>();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public void addItems(ArrayList<Kurs> results) {
        if (results != null && results.size() > 0) {
            listProduct.clear();
            listProduct.addAll(results);
            notifyDataSetChanged();
        }
    }

    public void removeItem(Kurs kurs) {
        listProduct.remove(kurs);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_prediction_kurs_list, viewGroup, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, final int i) {
        if (listProduct != null && listProduct.size() > 0) {
            final Kurs kurs = getItemAdapter(i);
            holder.tvInitial.setText("KR");
            holder.tvDate.setText(kurs.year);
            holder.tvKurs.setText(String.valueOf(kurs.nominal.intValue()));
            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onItemRemove(kurs, i);
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onItemClick(kurs, i);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listProduct != null ? listProduct.size() : 0;
    }

    public Kurs getItemAdapter(int position) {
        return listProduct.get(position);
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_initial)
        public TextView tvInitial;
        @BindView(R.id.tv_date)
        public TextView tvDate;
        @BindView(R.id.tv_kurs)
        public TextView tvKurs;
        @BindView(R.id.iv_delete)
        public ImageView ivDelete;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Kurs kurs, int pos);

        void onItemRemove(Kurs kurs, int pos);
    }
}
