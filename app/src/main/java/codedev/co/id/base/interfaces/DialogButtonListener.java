package codedev.co.id.base.interfaces;

public interface DialogButtonListener {
    void onPositiveDialogButtonListener();
    void onNegativeDialogButtonListener();
}
