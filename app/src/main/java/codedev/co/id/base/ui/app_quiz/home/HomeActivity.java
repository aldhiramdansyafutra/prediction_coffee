package codedev.co.id.base.ui.app_quiz.home;

import android.os.Bundle;
import android.support.annotation.Nullable;

import codedev.co.id.base.ui.base.BaseActivityWithoutToolbar;

public class HomeActivity extends BaseActivityWithoutToolbar {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDefaultFragment(new HomeFragment());
    }
}
