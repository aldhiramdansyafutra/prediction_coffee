package codedev.co.id.base.ui.app_prediction.product.form;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.data.model_prediction.Product;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.Extras;
import codedev.co.id.base.utils.StringHelper;

public class ProductFormFragment extends BaseFragment implements ProductFormMvpView {

    @BindView(R.id.til_product_id)
    TextInputLayout tilProductId;
    @BindView(R.id.til_product_name)
    TextInputLayout tilProductName;
    @BindView(R.id.til_price)
    TextInputLayout tilPrice;
    @BindView(R.id.til_item_sold)
    TextInputLayout tilItemSold;
    @BindView(R.id.til_month)
    TextInputLayout tilMonth;
    @BindView(R.id.til_year)
    TextInputLayout tilYear;

    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.btn_search)
    Button btnSearch;
    @BindView(R.id.btn_generate_id)
    Button btnGenerateId;

    @Inject
    ProductFormPresenter mPresenter;

    boolean isProductDetail;

    @Override
    protected void attachViewForPresenter() {
        mPresenter.attachView(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_prediction_form_product;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupView(getArguments());
    }

    private void setupView(Bundle bundle) {
        if (bundle != null && bundle.containsKey(Extras.PRODUCT_DETAIL)) {
            mPresenter.mProduct = bundle.getParcelable(Extras.PRODUCT_DETAIL);
            isProductDetail = mPresenter.mProduct != null;

            btnSubmit.setText("Submit");
            btnSearch.setVisibility(View.VISIBLE);
            btnGenerateId.setVisibility(View.VISIBLE);

            if (mPresenter.mProduct != null) {
                tilProductId.clearFocus();
                tilProductId.getEditText().setFocusable(false);
                tilProductId.getEditText().setText(mPresenter.mProduct.productId);
                tilProductName.requestFocus();
                tilProductName.getEditText().setText(mPresenter.mProduct.name);
                tilPrice.getEditText().setText(String.valueOf(mPresenter.mProduct.price));
                tilItemSold.getEditText().setText(String.valueOf(mPresenter.mProduct.soldItem));
                tilMonth.getEditText().setText(String.valueOf(mPresenter.mProduct.month));
                tilYear.getEditText().setText(mPresenter.mProduct.year);

                btnSubmit.setText("Update");
                btnSearch.setVisibility(View.GONE);
                btnGenerateId.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onSucceedCreateProduct() {
        dissmissLoading();
        getActivity().finish();
    }

    @Override
    public void onSucceedUpdateProduct() {
        dissmissLoading();
        getActivity().finish();
    }

    @Override
    public void onSucceedGetProduct(Product product) {
        dissmissLoading();
        tilProductName.getEditText().setText(product.name);
        tilPrice.getEditText().setText(String.valueOf(product.price));
        tilItemSold.getEditText().setText("");
        tilMonth.getEditText().setText("");
        tilYear.getEditText().setText(String.valueOf(product.year));
    }

    @Override
    public void onSucceedGenerateId() {
        dissmissLoading();
        tilProductId.getEditText().setText(mPresenter.generatedId);
    }

    @Override
    public void showProgressBarLoading() {
        showLoading();
    }

    @Override
    public void onFailedRequest() {
        dissmissLoading();
    }

    @OnClick({R.id.btn_submit, R.id.btn_search, R.id.btn_generate_id})
    void handleOnClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                if (mPresenter.validate(tilProductId, tilProductName, tilPrice, tilItemSold, tilMonth, tilYear)) {
                    if (isProductDetail) {
                        mPresenter.updateProduct();
                    } else {
                        mPresenter.submitProduct();
                    }
                }
                break;
            case R.id.btn_search:
                mPresenter.getProduct(StringHelper.getStringFromEditText(tilProductId.getEditText()));
                break;
            case R.id.btn_generate_id:
                mPresenter.generateId();
                break;
        }
    }
}
