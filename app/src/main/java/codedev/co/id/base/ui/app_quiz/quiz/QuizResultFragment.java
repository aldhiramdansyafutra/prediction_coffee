package codedev.co.id.base.ui.app_quiz.quiz;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.data.QuizResult;
import codedev.co.id.base.data.locale.PreferencesHelper;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.Extras;

public class QuizResultFragment extends BaseFragment {

    @BindView(R.id.tv_title)
    TextView mTextTitle;
    @BindView(R.id.tv_description)
    TextView mTextDescription;
    @BindView(R.id.tv_point)
    TextView mTextPoint;

    @Inject
    PreferencesHelper mPreferencesHelper;

    @Override
    protected void attachViewForPresenter() {

    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_app_quiz_result;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(Extras.QUIZ_RESULT)) {
            QuizResult result = getArguments().getParcelable(Extras.QUIZ_RESULT);
            if (result != null) {
                mTextTitle.setText(result.title);
                mTextDescription.setText(result.description);
                mTextPoint.setText(String.valueOf(result.point));
                savePointIntoPreference(result.quizLevelType, String.valueOf(result.point) );
            }
        }
    }

    @OnClick(R.id.btn_finish)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_finish:
                finishQuizAndGoToRootHomeScreen();
                break;
        }
    }

    private void savePointIntoPreference(QuizPresenter.QuizLevelType levelType, String point) {
        switch (levelType) {
            case EASY:
                mPreferencesHelper.putString(PreferencesHelper.PREF_QUIZ_LEVEL_EASY, point, "0");
                break;
            case MEDIUM:
                mPreferencesHelper.putString(PreferencesHelper.PREF_QUIZ_LEVEL_MEDIUM, point, "0");
                break;
            case HARD:
                mPreferencesHelper.putString(PreferencesHelper.PREF_QUIZ_LEVEL_HARD, point, "0");
                break;
        }
    }
}
