package codedev.co.id.base.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import codedev.co.id.base.R;
import codedev.co.id.base.interfaces.DialogButtonListener;

public final class DialogFactory {

    private static void handleButtonDialog(final AlertDialog dialog, String positiveButton, String negativeButton, final DialogButtonListener dialogButtonListener) {
        if (!TextUtils.isEmpty(positiveButton)) {
            dialog.setButton(AlertDialog.BUTTON_POSITIVE, positiveButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (dialogButtonListener != null) {
                        dialogButtonListener.onPositiveDialogButtonListener();
                    }
                    dialog.dismiss();
                }
            });
        }

        if (!TextUtils.isEmpty(negativeButton)) {
            dialog.setButton(AlertDialog.BUTTON_NEGATIVE, negativeButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (dialogButtonListener != null) {
                        dialogButtonListener.onNegativeDialogButtonListener();
                        dialog.dismiss();
                    }
                }
            });
        }
    }

    private static void handleButtonTextDialog(final AlertDialog dialog, final Context context) {
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button btnPositive = dialog.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setTextSize(ViewUtil.pxToDp(context.getResources().getDimension(R.dimen.text_size_normal)));
                btnPositive.setTextColor(ContextCompat.getColor(context, R.color.colorCicilGreen));


                Button btnNegative = dialog.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setTextSize(ViewUtil.pxToDp(context.getResources().getDimension(R.dimen.text_size_normal)));
                btnNegative.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        });
    }

    public static Dialog createDialog(final Context context, String title, String message, String positiveButton, String negativeButton, final DialogButtonListener dialogButtonListener) {
        return dialogWithImage(context, title, message, positiveButton, negativeButton, 0, dialogButtonListener);
    }

    public static Dialog dialogWithImage(final Context context, String title, String message, String positiveButton, String negativeButton, int imageRes1, final DialogButtonListener dialogButtonListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        handleButtonDialog(dialog, positiveButton, negativeButton, dialogButtonListener);

        handleButtonTextDialog(dialog, context);

        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.dialog_with_image, null);
        TextView textTitle = ButterKnife.findById(view, R.id.dialog_title);
        TextView textMessage = ButterKnife.findById(view, R.id.dialog_message);
        ImageView imageView = ButterKnife.findById(view, R.id.dialog_imageview);

        if (!TextUtils.isEmpty(title)) {
            textTitle.setText(title);
        } else {
            textTitle.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(message)) {
            textMessage.setText(message);
        } else {
            textMessage.setVisibility(View.GONE);
        }

        if (imageRes1 == 0) {
            imageView.setVisibility(View.GONE);
        } else {
            imageView.setImageResource(imageRes1);
        }

        dialog.setView(view);

        return dialog;
    }

    public static Snackbar createSnackbar(Context context, View view, String message, int color) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(context, color));
        TextView tv = sbView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(ContextCompat.getColor(context, android.R.color.white));
        tv.setMaxLines(3);
        return snackbar;
    }

    public static Snackbar errorSnackbar(Context context, View view, String message) {
        return createSnackbar(context, view, message, R.color.colorErrorBackground);
    }
}