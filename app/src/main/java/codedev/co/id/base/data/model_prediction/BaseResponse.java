package codedev.co.id.base.data.model_prediction;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    @SerializedName("code")
    public int code;
    @SerializedName("message")
    public String message;
}
