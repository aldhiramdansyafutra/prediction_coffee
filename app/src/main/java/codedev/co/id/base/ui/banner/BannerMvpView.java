package codedev.co.id.base.ui.banner;

/**
 * Created by franky on 6/13/17.
 */

public interface BannerMvpView {
    void onSucceedGetBannerList();
}
