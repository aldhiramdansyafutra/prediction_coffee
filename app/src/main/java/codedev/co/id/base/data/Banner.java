package codedev.co.id.base.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by franky on 3/16/17.
 */

public class Banner implements Parcelable {

    public class Result {
        @SerializedName("data_list")
        @Expose
        public List<Banner> bannerList;
    }

    public static String bannerObject;

    @SerializedName("id")
    @Expose
    public Integer id;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("sort_order")
    @Expose
    public Integer sortOrder;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("target_url")
    @Expose
    public String targetUrl;

    @SerializedName("on_desktop")
    @Expose
    public Boolean onDesktop;

    @SerializedName("on_mobile")
    @Expose
    public Boolean onMobile;

    // For banner preform v2
    @SerializedName("title")
    @Expose
    public String title;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.status);
        dest.writeValue(this.sortOrder);
        dest.writeString(this.description);
        dest.writeString(this.image);
        dest.writeString(this.targetUrl);
        dest.writeValue(this.onDesktop);
        dest.writeValue(this.onMobile);
        dest.writeString(this.title);
    }

    public Banner() {
    }

    protected Banner(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.status = in.readString();
        this.sortOrder = (Integer) in.readValue(Integer.class.getClassLoader());
        this.description = in.readString();
        this.image = in.readString();
        this.targetUrl = in.readString();
        this.onDesktop = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.onMobile = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.title = in.readString();
    }

    public static final Parcelable.Creator<Banner> CREATOR = new Parcelable.Creator<Banner>() {
        @Override
        public Banner createFromParcel(Parcel source) {
            return new Banner(source);
        }

        @Override
        public Banner[] newArray(int size) {
            return new Banner[size];
        }
    };
}
