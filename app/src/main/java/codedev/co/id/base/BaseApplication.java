package codedev.co.id.base;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDelegate;

import codedev.co.id.base.injection.component.ApplicationComponent;
import codedev.co.id.base.injection.component.DaggerApplicationComponent;
import codedev.co.id.base.injection.module.ApplicationModule;
import codedev.co.id.base.services.ConnectivityService;
import dagger.internal.DaggerCollections;

public class BaseApplication extends Application {

    ApplicationComponent mApplicationComponent;
    public static Context mContext;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        getComponent().inject(this);
        mContext = this;
    }

    public static Context getContext() {
        return mContext;
    }

    public static BaseApplication get(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }

    public void setConnectivityListener(ConnectivityService.ConnectivityReceiverListener listener) {
        ConnectivityService.connectivityReceiverListener = listener;
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
        }

        return mApplicationComponent;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
