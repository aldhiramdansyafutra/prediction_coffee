package codedev.co.id.base.ui.app_location.maps;

import codedev.co.id.base.interfaces.BaseView;

public interface LocationMapsView extends BaseView {
    void onSucceedGetLocation();
}
