package codedev.co.id.base.utils;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import codedev.co.id.base.BaseApplication;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by franky on 11/8/16.
 */

public class StringHelper {

    public static final String DEFAULT_EXAMPLE_PRODUCT_URL = "https://www.tokopedia.com/miband/xiaomi-yi-action-camera-wifi";

    private static final Pattern urlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    public static String getUrlFromString(String url) {
        Matcher matcher = urlPattern.matcher(url);
        while (matcher.find()) {
            int matchStart = matcher.start(1);
            int matchEnd = matcher.end();
            return url.substring(matchStart, matchEnd);
        }
        return null;
    }

    public static String addSeparatorForNumber(String value) {
        DecimalFormat decimalFormat = new DecimalFormat();
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();

        decimalFormatSymbols.setGroupingSeparator('.');
        decimalFormat.setMaximumFractionDigits(0);
        decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);

        if (TextUtils.isEmpty(value)) {
            return decimalFormat.format(0);
        } else {
            return decimalFormat.format(Double.parseDouble(value));
        }
    }

    public static boolean validUrlFormat(String url) {
        return StringHelper.isNotEmpty(url) && (url.startsWith("http://") || url.startsWith("https://"));
    }

    public static String convertToCurrencyFormatIDR(String price) {
        return convertToCurrencyFormat(price, true);
    }

    public static String convertToCurrencyFormat(String price, boolean shouldAddCurrency) {
        Locale locale = new Locale("in","ID");
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(locale);
        symbols.setGroupingSeparator('.');
        symbols.setMonetaryDecimalSeparator(',');

        if (shouldAddCurrency) {
            symbols.setCurrencySymbol(symbols.getCurrencySymbol() + " ");
        } else {
            symbols.setCurrencySymbol("");
        }

        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
        DecimalFormat kursIndonesia = new DecimalFormat(df.toPattern(), symbols);
        kursIndonesia.setMaximumFractionDigits(0);

        if (TextUtils.isEmpty(price)) {
            return kursIndonesia.format(0);
        } else {
            return kursIndonesia.format(Double.parseDouble(price));
        }
    }

    public static String convertDoubleWithoutDecimal(double value) {
        return new DecimalFormat("#").format(Math.ceil(value));
    }

    public static String convertBooleanToString(boolean isTrue) {
        return isTrue ? "true" : "false";
    }

    public static boolean convertStringToBoolean(String string) {
        return string.equalsIgnoreCase("Ya") || string.equalsIgnoreCase("true");
    }

    public static String getStringFromEditText(EditText editText) {
        return editText.getText().toString().trim();
    }

    public static String getFilename(String filepath) {
        if (filepath == null)
            return null;

        final String[] filepathParts = filepath.split("/");

        return filepathParts[filepathParts.length - 1];
    }

    public static String hideEmailValue (String email) {
        String[] splitEmail = email.split("@");

        String firstHide = splitEmail[0].charAt(0)+"****"+splitEmail[0].charAt(splitEmail[0].length()-1);
        String secondHide = splitEmail[1].substring(0, splitEmail[1].indexOf("."));
        secondHide = secondHide.charAt(0)+"****"+splitEmail[1].substring(splitEmail[1].indexOf("."));
        return firstHide+"@"+secondHide;
    }

    public static boolean isNotEmpty(CharSequence str) {
        return !isEmpty(str);
    }

    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    public static String convertCalendarToString (Calendar calendar, String outputFormat) {
        SimpleDateFormat df = new SimpleDateFormat(outputFormat, new Locale("in", "ID"));
        df.setTimeZone(TimeZone.getDefault());
        return df.format(calendar.getTime());
    }

    public static String convertCalendarToString(Calendar calendar) {
        return convertCalendarToString(calendar, Config.NORMAL_DATE_FORMAT);
    }

    public static String getCalendarFieldWithFormat(String stringFormat, Date date) {
        return (String) android.text.format.DateFormat.format(stringFormat, date);
    }

    public static String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {

            InputStream is = context.getAssets().open(fileName);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    public static String censoredString(String value, boolean isPhone) {
        if (isNotEmpty(value)) {
            value = value.trim();
            int length = value.length();

            if (isNotEmpty(value)) {
                if (isPhone && value.charAt(0) == '0') {
                    value = value.replaceFirst("0", Config.INDONESIA_PHONE_CODE);
                }

                if (length > 5) {
                    int lengthReplace = value.substring(6, length-1).length();
                    String stringReplace = "";

                    for (int i=0; i<lengthReplace;i++) {
                        stringReplace = stringReplace + "*";
                    }

                    value = value.substring(0, 6) + stringReplace + value.substring(length-1);
                }
            }
        }

        return value;
    }

    /**
     * Remove 0 at first in phone, because it should be +62...
     * By default it will have +62 when user input their phone, but when user type +620, it will remove 0 when we send it to server.
     * @param phone
     * @return
     */
    public static String removeZeroFirstDigitPhone(String phone) {
        if (phone.length() > 3 && Character.getNumericValue(phone.charAt(3))  == 0) {
            phone = phone.replaceFirst("0","");
        }

        return phone;
    }

    public static String getHexaStringFromColorsXml(@ColorRes int colorsXml) {
        return "#" + Integer.toHexString(ContextCompat.getColor(BaseApplication.getContext(), colorsXml) & 0x00ffffff);
    }

    public static void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink,
                    startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setHighlightColor(
                Color.TRANSPARENT); // prevent TextView change background when highlight
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    public static void stringToLowerCase(EditText editText, String text) {
        if (!text.equals(text.toLowerCase())) {
            text = text.toLowerCase();
            editText.setText(text.toLowerCase());
            editText.setSelection(editText.getText().toString().length());
        }
    }

    public static String formatPhoneNumber(String phoneNumber) {
        if (phoneNumber != null && phoneNumber.startsWith("0")) {
            return "+62" + phoneNumber.substring(1);
        }

        return phoneNumber;
    }

    public static String encodeTextMessage(String message) {
        try {
            return URLEncoder.encode(message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return message;
        }
    }

    public static String getStringFirstNChar(String source, int n) {
       return source.substring(0, Math.min(source.length(), n));
    }

    public static RequestBody convertToRequestBody(JsonObject jsonObject) {
        return RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
    }

}
