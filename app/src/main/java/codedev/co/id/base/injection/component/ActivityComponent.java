package codedev.co.id.base.injection.component;

import android.content.Context;

import codedev.co.id.base.injection.ActivityContext;
import codedev.co.id.base.injection.PerActivity;
import codedev.co.id.base.injection.module.ActivityModule;
import codedev.co.id.base.ui.app_location.list.SchoolLocationActivity;
import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    @ActivityContext
    Context context();

    void inject(SchoolLocationActivity schoolLocationActivity);

}
