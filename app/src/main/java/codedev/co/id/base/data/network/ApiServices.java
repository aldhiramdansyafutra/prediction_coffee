package codedev.co.id.base.data.network;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import codedev.co.id.base.BuildConfig;
import codedev.co.id.base.data.model_prediction.BaseResponse;
import codedev.co.id.base.data.model_prediction.Conversion;
import codedev.co.id.base.data.model_prediction.ConversionRequest;
import codedev.co.id.base.data.model_prediction.GenerateId;
import codedev.co.id.base.data.model_prediction.Kurs;
import codedev.co.id.base.data.model_prediction.PopularPrediction;
import codedev.co.id.base.data.model_prediction.Product;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiServices {

    // ============ Product Endpoint ============
    @GET("product/read.php")
    Call<Product.Result> getAllProduct();

    @GET("product/generateId.php")
    Call<GenerateId> generateProductId();

    @POST("product/findById.php")
    Call<Product.Result> getProduct(@Body RequestBody body);

    @POST("product/create.php")
    Call<BaseResponse> createProduct(@Body Product product);

    @POST("product/update.php")
    Call<BaseResponse> updateProduct(@Body Product product);

    @POST("product/delete.php")
    Call<BaseResponse> deleteProduct(@Body RequestBody id);

    @GET("product/chart_data.php")
    Call<PopularPrediction.Result> getPopularPrediction(
            @Query("year") String year,
            @Query("limit") String limit
    );


    // ============ Kurs Endpoint ============
    @GET("soldItem/read.php")
    Call<Kurs.Result> getKurs();


    // ============ Conversion Endpoint ============
    @POST("product/convertion.php")
    Call<Conversion> getConversion(@Body ConversionRequest request);


    class Factory {
        public static ApiServices makeApiService(Context context) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                    : HttpLoggingInterceptor.Level.NONE);

            OkHttpClient okHttpClient =
                    new OkHttpClient.Builder()
                            .addNetworkInterceptor(logging)
                            .readTimeout(180, TimeUnit.SECONDS)
                            .connectTimeout(180, TimeUnit.SECONDS)
                            .writeTimeout(180, TimeUnit.SECONDS)
                            .addInterceptor(new Interceptor() {
                                @Override
                                public Response intercept(@NonNull Chain chain) throws IOException {
                                    Request request = chain.request();
                                    request = request.newBuilder()
                                            .header("Content-Type", "application/json").build();
                                    return chain.proceed(request);
                                }
                            })
                            .build();

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
            Retrofit retrofit = new Retrofit.Builder().baseUrl(Host.getCurrentHost())
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            return retrofit.create(ApiServices.class);
        }
    }
}
