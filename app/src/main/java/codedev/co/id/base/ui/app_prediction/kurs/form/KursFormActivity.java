package codedev.co.id.base.ui.app_prediction.kurs.form;

import android.os.Bundle;
import android.support.annotation.Nullable;

import codedev.co.id.base.ui.app_prediction.product.form.ProductFormFragment;
import codedev.co.id.base.ui.base.BaseActivityWithToolbar;

public class KursFormActivity extends BaseActivityWithToolbar {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupFragment(getIntent().getExtras());
    }

    private void setupFragment(Bundle bundle) {
        KursFormFragment fragment = new KursFormFragment();
        fragment.setArguments(bundle);
        setupDefaultFragment(fragment);
    }
}
