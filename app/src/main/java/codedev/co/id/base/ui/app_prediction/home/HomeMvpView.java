package codedev.co.id.base.ui.app_prediction.home;

import codedev.co.id.base.data.model_prediction.PopularPrediction;
import codedev.co.id.base.interfaces.BaseRequestMvpView;

public interface HomeMvpView extends BaseRequestMvpView {
    void onSucceedGetProduct(PopularPrediction.Result results);
}
