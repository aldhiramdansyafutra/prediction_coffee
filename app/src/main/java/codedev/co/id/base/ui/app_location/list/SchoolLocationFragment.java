package codedev.co.id.base.ui.app_location.list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import codedev.co.id.base.R;
import codedev.co.id.base.constant.Constant;
import codedev.co.id.base.data.Location;
import codedev.co.id.base.ui.app_location.list.detail.SchoolListLocationDetailActivity;
import codedev.co.id.base.ui.base.BaseActivity;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.Extras;

public class SchoolLocationFragment extends BaseFragment implements SchoolLocationView {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.et_search)
    TextInputEditText etSearch;

    @Inject
    SchoolLocationPresenter mPresenter;

    private View mViewRoot;
    private SchoolLocationAdapter mAdapter;

    @Override
    protected int getLayout() {
        return R.layout.activity_app_location_list_schools;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mViewRoot = super.onCreateView(inflater, container, savedInstanceState);
        etSearch.clearFocus();
        mRecyclerView.requestFocus();
        return mViewRoot;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressBar = ButterKnife.findById(mViewRoot, R.id.layout_progress);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponent();
        mPresenter.getLocation(getActivity());
    }

    @Override
    protected void attachViewForPresenter() {
        mPresenter.attachView(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    private void initComponent() {
        mAdapter = new SchoolLocationAdapter(getActivity(), R.layout.item_app_location_school_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new SchoolLocationAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Location.Data item, int position) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Extras.DETAIL_LOCATION, item);
                bundle.putString(Extras.IMAGE_URL, item.image);
                BaseActivity.navigateActivity(getActivity(), bundle, R.string.school_list_detail, SchoolListLocationDetailActivity.class);
                animationOpenActivity();
            }
        });
    }

    @Override
    public void onSucceedGetLocation() {
        mAdapter.addData(mPresenter.locationList);
    }

    @Override
    public void showLoading(Constant.LoadingType loadingType) {
        handlingPrgogressbar(loadingType);
    }

    @Override
    public void onError(String errMessage) {
        dissmissLoading();
    }

    @OnTextChanged(value = {R.id.et_search}, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void handleTextChanged(CharSequence charSequence) {
        String text = charSequence.toString();
        if (text.length() != 0) {
            mAdapter.getFilter().filter(charSequence);
        }
    }

    @OnClick(R.id.iv_clear_search)
    void handleOnClick() {
        etSearch.setText("");
        mPresenter.getLocation(getActivity());
    }
}
