package codedev.co.id.base.ui.app_quiz.home;

import javax.inject.Inject;

import codedev.co.id.base.data.locale.PreferencesHelper;
import codedev.co.id.base.ui.app_quiz.quiz.QuizPresenter;

public class HomePresenter {

    @Inject
    PreferencesHelper preferencesHelper;

    @Inject
    public HomePresenter() {
    }

    public boolean isQuizAvailable(QuizPresenter.QuizLevelType levelType) {
        int score = 60;
        switch (levelType) {
            case EASY:
                return true;
            case MEDIUM:
                return isContentAvailable(preferencesHelper.getString(PreferencesHelper.PREF_POINT_LEVEL_EASY, "0"), score);
            case HARD:
                score = 70;
                boolean isAvailEasy = isContentAvailable(preferencesHelper.getString(PreferencesHelper.PREF_POINT_LEVEL_MEDIUM, "0"), 60);
                boolean isAvailMedium = isContentAvailable(preferencesHelper.getString(PreferencesHelper.PREF_POINT_LEVEL_MEDIUM, "0"), score);
                return isAvailEasy && isAvailMedium;
        }
        return true;
    }

    private boolean isContentAvailable(String point, int minScore) {
        double score = Double.parseDouble(point);
        return score >= minScore;
    }
}
