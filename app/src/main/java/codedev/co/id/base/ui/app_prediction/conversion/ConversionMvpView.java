package codedev.co.id.base.ui.app_prediction.conversion;

import codedev.co.id.base.data.model_prediction.Conversion;
import codedev.co.id.base.interfaces.BaseRequestMvpView;

public interface ConversionMvpView extends BaseRequestMvpView {
    void onSucceed(Conversion result);
    void onPredictionSucceed();
}
