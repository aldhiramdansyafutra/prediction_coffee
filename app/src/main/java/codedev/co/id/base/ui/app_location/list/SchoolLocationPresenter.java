package codedev.co.id.base.ui.app_location.list;

import android.content.Context;
import android.os.Handler;

import com.google.gson.Gson;

import java.util.ArrayList;

import javax.inject.Inject;

import codedev.co.id.base.constant.Constant;
import codedev.co.id.base.data.Location;
import codedev.co.id.base.ui.base.BasePresenter;
import codedev.co.id.base.utils.StringHelper;

public class SchoolLocationPresenter extends BasePresenter<SchoolLocationView> {

    public ArrayList<Location.Data> locationList;

    @Inject
    public SchoolLocationPresenter() {

    }

    public void getLocation(final Context context) {
        if (isViewAttached()) {
            mMvpView.showLoading(Constant.LoadingType.SHOW);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        String json = StringHelper.loadJSONFromAsset(context, "location.json");
                        Location locationResult = new Gson().fromJson(json, Location.class);
                        locationList = locationResult.list;
                        mMvpView.onSucceedGetLocation();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        mMvpView.showLoading(Constant.LoadingType.HIDE);
                    }
                }
            }, 3000);
        }
    }

}
