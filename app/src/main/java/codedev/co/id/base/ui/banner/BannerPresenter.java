package codedev.co.id.base.ui.banner;

import android.content.Context;

import com.google.gson.Gson;

import codedev.co.id.base.data.Banner;
import codedev.co.id.base.utils.StringHelper;

public class BannerPresenter {

    public Context mContext;
    public Banner.Result mBannerList;
    private BannerMvpView mMvpView;

    public BannerPresenter(Context context, BannerMvpView view) {
        this.mContext = context;
        this.mMvpView = view;
    }

    public void getBanners() {
        String json = StringHelper.loadJSONFromAsset(mContext, "preform_banner.json");
        Banner.Result item = new Gson().fromJson(json, Banner.Result.class);
        if (item != null && item.bannerList != null && !item.bannerList.isEmpty()) {
            mBannerList = item;
            mMvpView.onSucceedGetBannerList();
        }
    }
}
