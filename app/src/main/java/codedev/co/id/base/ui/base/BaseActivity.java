package codedev.co.id.base.ui.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatDelegate;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.luseen.simplepermission.permissions.PermissionActivity;

import butterknife.ButterKnife;
import codedev.co.id.base.BaseApplication;
import codedev.co.id.base.R;
import codedev.co.id.base.injection.component.ActivityComponent;
import codedev.co.id.base.injection.component.DaggerActivityComponent;
import codedev.co.id.base.injection.module.ActivityModule;
import codedev.co.id.base.services.ConnectivityService;
import codedev.co.id.base.ui.MainActivity;
import codedev.co.id.base.ui.app_location.home.HomeActivity;
import codedev.co.id.base.ui.expand_menu.ExpandedMenuDialogFragment;
import codedev.co.id.base.utils.Extras;
import codedev.co.id.base.utils.ViewUtil;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public abstract class BaseActivity extends PermissionActivity implements ConnectivityService.ConnectivityReceiverListener {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public enum SnackbarType {
        ERROR, WARNING, DEFAULT
    }

    protected TextView mTextHeaderNoConnection;

    ConnectivityService mConnectivityReceiver;
    private ActivityComponent mActivityComponent;
    private IntentFilter mIntentFilter;
    private Dialog forceUpdateDialog;
    private View mSnackBarContainer;

    public static final int DEFAULT_SNACKBAR_DURATION = 3000;

    public static Intent getStartIntent(Context context, Bundle bundle, Class activityClass) {
        Intent intent = new Intent(context, activityClass);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        return intent;
    }

    public static void navigateActivity(Activity activity, Bundle bundle, @StringRes int title, Class activityClass) {
        if (bundle == null) bundle = new Bundle();
        bundle.putInt(Extras.TITLE_ID, title);
        Intent intent = BaseActivity.getStartIntent(activity, bundle, activityClass);
        activity.startActivity(intent);
    }

    public static void navigateActivity(Activity activity, @StringRes int title, Class activityClass) {
        navigateActivity(activity, null, title, activityClass);
    }

    public void setupDefaultFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment, fragment.getTag()).commit();
    }

    public ActivityComponent activityComponent() {
        if (mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(BaseApplication.get(this).getComponent())
                    .build();
        }
        return mActivityComponent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusbarColor(getToolbarColorId());

        int contentViewID = getContentView();
        if (contentViewID != 0) {
            setContentView(contentViewID);
            ButterKnife.bind(this);
            mIntentFilter = new IntentFilter();
            mIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            mConnectivityReceiver = new ConnectivityService();

            int titleBarId = getTitleBar();
            ActionBar actionBar = getSupportActionBar();
            if (titleBarId != 0 && actionBar != null) {
                actionBar.setTitle(getString(getTitleBar()));
            }

            int toolbarColorId = getToolbarColorId();
            if (toolbarColorId != 0 && actionBar != null) {
                actionBar.setBackgroundDrawable(new ColorDrawable(toolbarColorId));
            }
        }
    }

    public void animationOpenActivity() {
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    public void animationCloseActivity() {
        overridePendingTransition(R.anim.anim_slide_out_right, R.anim.anim_slide_in_right);
    }

    public void animationOpenUpActivity() {
        overridePendingTransition(R.anim.anim_slide_up, R.anim.stay);
    }

    public void animationCloseDownAcitivity() {
        overridePendingTransition(R.anim.stay, R.anim.anim_slide_down);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ViewUtil.hideKeyboard(this);
        int id = item.getItemId();
        switch (id) {
            case R.id.expanded_menu:
//                gotoSearchActivity();
                showActionBarDialog(VISIBLE);
                break;
//            case R.id.menu_cart:
//                goToCartActivity();
//                hashMapTracking.put("menu", "order");
//                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

//    private void goToCartActivity() {
//        Bundle bundle = new Bundle();
//        bundle.putInt(Extras.FRAGMENT, FragmentIndex.FRAGMENT_HISTORY_ORDER);
//        bundle.putInt(Extras.TITLE_ID, R.string.my_order);
//        Intent intent = BaseActivity.getStartIntent(this, bundle, OrderActivity.class);
//        startActivity(intent);
//        animationOpenActivity();
//    }
//
//    private void gotoSearchActivity() {
//        Intent intent = BaseActivity.getStartIntent(this, null, SearchActivity.class);
//        startActivity(intent);
//        animationOpenActivity();
//    }

    protected void showActionBarDialog(int visibleTuition) {
        Bundle bundle = new Bundle();
        bundle.putInt(Extras.IS_FROM_TUITION_SIMULATION, visibleTuition);
        ExpandedMenuDialogFragment expandedMenuDialogFragment = ExpandedMenuDialogFragment.newInstance(bundle);
        expandedMenuDialogFragment.show(getSupportFragmentManager(), "");
    }

    public void finishAllActivityAndGoToRootHomeScreen() {
        Intent intentMain = BaseActivity.getStartIntent(this, null, HomeActivity.class);
        startActivity(intentMain);
        this.finishAffinity();
        animationCloseActivity();
    }

    public void finishQuizAndGoToRootHomeScreen() {
        Intent intentMain = BaseActivity.getStartIntent(this, new Bundle(), codedev.co.id.base.ui.app_quiz.home.HomeActivity.class);
        startActivity(intentMain);
        this.finishAffinity();
        animationCloseActivity();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        animationCloseActivity();
    }

    public static void tintMenuIcon(Context context, MenuItem item, @ColorRes int color) {
        if (item != null) {
            Drawable normalDrawable = item.getIcon();
            Drawable wrapDrawable = DrawableCompat.wrap(normalDrawable);
            DrawableCompat.setTint(wrapDrawable, context.getResources().getColor(color));

            item.setIcon(wrapDrawable);
        }
    }

    // Method to manually check connection status
    protected void checkConnection() {
        boolean isConnected = ConnectivityService.isConnected();
        onNetworkConnectionChanged(isConnected);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseApplication.get(BaseActivity.this).setConnectivityListener(this);
        registerReceiver(mConnectivityReceiver, mIntentFilter);
        checkConnection();
        invalidateOptionsMenu();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mConnectivityReceiver);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (mTextHeaderNoConnection != null) {
            if (isConnected) {
                mTextHeaderNoConnection.setVisibility(GONE);
            } else {
                mTextHeaderNoConnection.setVisibility(View.VISIBLE);
            }
        }
    }

    public void goToMarket() {

        Intent market;

        try {
            market = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + getPackageName()));
        } catch (android.content.ActivityNotFoundException e) {
            market = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName()));
        }
        startActivity(market);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected abstract int getContentView();

    protected abstract int getTitleBar();

    protected abstract int getToolbarColorId();

    protected void setStatusbarColor(int colorInt) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //if (colorInt == 0) colorInt = R.color.colorCicilGreen;
            getWindow().setStatusBarColor(ActivityCompat.getColor(this, colorInt));
        }
    }
}