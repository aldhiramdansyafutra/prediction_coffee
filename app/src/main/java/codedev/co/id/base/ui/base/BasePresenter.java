package codedev.co.id.base.ui.base;

import codedev.co.id.base.data.DataManager;
import codedev.co.id.base.interfaces.MvpView;
import codedev.co.id.base.interfaces.Presenter;

public class BasePresenter<V extends MvpView> implements Presenter<V> {

    public V mMvpView;
    public DataManager mDataManager;

    @Override
    public void attachView(V view) {
        mMvpView = view;
    }

    @Override
    public void detachView() {
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }
}
