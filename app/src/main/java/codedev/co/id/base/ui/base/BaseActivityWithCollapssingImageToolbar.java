package codedev.co.id.base.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import codedev.co.id.base.R;
import codedev.co.id.base.interfaces.HandlingCollapsingImageToolbarListener;
import codedev.co.id.base.utils.Extras;
import codedev.co.id.base.utils.Tools;

public abstract class BaseActivityWithCollapssingImageToolbar extends BaseActivityWithToolbar implements HandlingCollapsingImageToolbarListener {

    @BindView(R.id.iv_header)
    ImageView mImageHeader;
    @BindView(R.id.collapse_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    @BindView(R.id.appbar)
    AppBarLayout mAppBarLayout;

    @Override
    protected int getToolbarColorId() {
        return android.R.color.transparent;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_common_collapsing_image_toolbar;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusbarColor(getCollapsableToolbarColor() == 0 ? R.color.colorPrimary : getCollapsableToolbarColor());
        if (getImageToolbar() != 0) {
            mImageHeader.setImageResource(getImageToolbar());

        }
        mCollapsingToolbarLayout.setContentScrimResource(getCollapsableToolbarColor() == 0 ? R.color.colorPrimary : getCollapsableToolbarColor());
//        setAppbarSize();
        displayTitleWhenCollapse();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(Extras.IMAGE_URL)) {
            Tools.displayImageOriginal(this, mImageHeader, bundle.getString(Extras.IMAGE_URL));
        }
    }

//    private void setAppbarSize() { //if you want to change dynamically
//        if (getSizeAppbar() > 0) {
//            AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) mCollapsingToolbarLayout.getLayoutParams();
//            params.height = getSizeAppbar();
//            mCollapsingToolbarLayout.setLayoutParams(params);
//            mCollapsingToolbarLayout.requestLayout();
//        }
//    }

    private void displayTitleWhenCollapse() {
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    mCollapsingToolbarLayout.setTitle(getString(getTitleBar()));
                    isShow = true;
                } else if (isShow) {
                    mCollapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });
    }

    @Override
    public void doExpanded(boolean isExpanded) {
        mAppBarLayout.setExpanded(isExpanded, false);
        mImageHeader.setVisibility(!isExpanded ? View.INVISIBLE : View.VISIBLE);
    }

    protected abstract int getImageToolbar();

// untuk sekarang jangan dihapous dulu,
//    protected abstract int getSizeAppbar(); //in pixel

    protected abstract int getCollapsableToolbarColor();
}
