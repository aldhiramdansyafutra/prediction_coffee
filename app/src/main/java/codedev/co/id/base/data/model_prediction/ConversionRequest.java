package codedev.co.id.base.data.model_prediction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConversionRequest {
    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("year")
    @Expose
    public String year;

    public ConversionRequest() {
    }
}
