package codedev.co.id.base.ui.app_location.home;

import android.view.View;

import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.ui.app_location.list.SchoolLocationActivity;
import codedev.co.id.base.ui.app_location.maps.LocationMapsActivity;
import codedev.co.id.base.ui.base.BaseActivity;
import codedev.co.id.base.ui.base.BaseFragment;

public class HomeFragment extends BaseFragment {

    @Override
    protected void attachViewForPresenter() {

    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_app_location_home_location;
    }

    @OnClick({R.id.rl_maps, R.id.rl_kids, R.id.rl_school})
    public void handleOnClick(View view) {
        switch (view.getId()) {
            case R.id.rl_maps:
                BaseActivity.navigateActivity(getActivity(), R.string.title_activity_location_maps, LocationMapsActivity.class);
                animationOpenActivity();
                break;
            case R.id.rl_kids:
                goToWeb("https://www.haibunda.com/parenting/20181009194903-61-27190/5-hal-yang-dipelajari-anak-di-taman-kanak-kanak");
                break;
            case R.id.rl_school:
                BaseActivity.navigateActivity(getActivity(), R.string.school_list, SchoolLocationActivity.class);
                animationOpenActivity();
                break;
        }
    }
}
