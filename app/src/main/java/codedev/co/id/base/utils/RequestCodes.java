package codedev.co.id.base.utils;

/**
 * Created by franky on 11/28/16.
 */

public class RequestCodes {
    public static final int GO_TO_CAROUSELL_ACTIVITY = 1;
    public static final int GO_TO_PROFILE_ACTIVITY = 2;
    public static final int GO_TO_PASSWORD_ACTIVITY = 3;
    public static final int IMAGE_CAPTURE = 4;
    public static final int GO_TO_SUGGESTION_ACTIVITY = 5;
    public static final int GO_TO_ORDER_ACTIVITY = 6;
    public static final int GO_TO_PAYMENT_ACTIVITY = 7;
    public static final int GO_TO_OTP = 8;
    public static final int GO_TO_AMBASSADOR_USER_DETAIL = 9;
    public static final int GO_TO_AMBASSADOR_USER_ACTION_PHOTO = 10;
    public static final int GO_TO_AMBASSADOR_USER_ACTION_SCHEDULE = 11;
    public static final int GO_TO_NEW_USER_LIST = 12;
    public static final int GO_TO_EXISTING_USER_LIST = 13;
    public static final int GO_TO_VERIFY_EMAIL = 14;
    public static final int GO_TO_REGISTER_CONFIRMATION = 15;
    public static final int GO_TO_SETTINGS = 16;
    public static final int GO_TO_VOUCHER_LIST = 17;
    public static final int IMAGE_GALLERY = 18;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 19;
    public static final int SMARTLOCK_RC_SAVE = 20;
    public static final int SMARTLOCK_RC_READ = 21;
    public static final int IMAGE_CAPTURE_CAMERA_2 = 22;
    public static final int CAMERA_PERMISSION_RESULT = 23;
    public static final int GO_TO_ZXING_GENERATOR = 24;
    public static final int GO_TO_SET_MEETUP = 25;
    public static final int GO_TO_ZXING_SCANNER = 26;
    public static final int GO_TO_ORDER_DETAIL = 27;
    public static final int GO_TO_SIGNING_AGREEMENT = 28;
    public static final int GO_TO_PROFILE_TUITION = 29;
}
