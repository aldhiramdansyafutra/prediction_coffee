package codedev.co.id.base.ui.app_location.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;

import codedev.co.id.base.ui.base.BaseActivityWithToolbar;

public class HomeActivity extends BaseActivityWithToolbar {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDefaultFragment(new HomeFragment());
    }
}
