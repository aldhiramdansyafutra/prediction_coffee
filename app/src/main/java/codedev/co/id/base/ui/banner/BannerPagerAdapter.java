package codedev.co.id.base.ui.banner;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import codedev.co.id.base.data.Banner;
import id.co.codedev.wrappingviewpager.WrappingFragmentPagerAdapter;

public class BannerPagerAdapter extends WrappingFragmentPagerAdapter {
    private Banner.Result mBanner;

    public BannerPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return mBanner.bannerList == null ? 0 : mBanner.bannerList.size();
    }

    public void setBannerList(Banner.Result bannerImageList) {
        mBanner = bannerImageList;
    }

    @Override
    public Fragment getItem(int position) {
        return BannerItemFragment.newInstance(mBanner.bannerList.get(position), position);
    }

    @Override
    public float getPageWidth(int position) {
        return 0.95f;
    }
}