package codedev.co.id.base.ui.app_prediction.product.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.data.model_prediction.Product;
import codedev.co.id.base.ui.app_prediction.product.form.ProductFormActivity;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.Extras;

public class ProductFragment extends BaseFragment implements ProductMvpView, ProductAdapter.OnItemClickListener {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerProduct;
    @BindView(R.id.et_search)
    TextInputEditText etSearch;

    @Inject
    ProductPresenter mPresenter;

    ProductAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
        mAdapter = new ProductAdapter();
        mAdapter.setOnItemClickListener(this);
    }

    @Override
    protected void attachViewForPresenter() {
        mPresenter.attachView(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_prediction_product_list;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        etSearch.clearFocus();
        mRecyclerProduct.requestFocus();
        mRecyclerProduct.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecyclerProduct.setHasFixedSize(true);
        mRecyclerProduct.setAdapter(mAdapter);
        mPresenter.getProducts();
    }

    @OnClick(R.id.fab_add_product)
    void handleNewProduct(View view) {
        goToDetail(null, R.string.product_new);
    }

    private void goToDetail(Product product, int title) {
        Intent intent = new Intent(getActivity(), ProductFormActivity.class);
        intent.putExtra(Extras.TITLE_ID, title);
        intent.putExtra(Extras.PRODUCT_DETAIL, product);
        startActivity(intent);
    }

    @Override
    public void onItemClick(Product product, int pos) {
        goToDetail(product, R.string.product_detail);
    }

    @Override
    public void onItemRemove(Product product, int pos) {
        mAdapter.removeItem(product);
    }

    @Override
    public void onSucceedGetProducts(ArrayList<Product> results) {
        mAdapter.addItems(results);
    }
}
