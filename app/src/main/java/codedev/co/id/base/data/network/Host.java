package codedev.co.id.base.data.network;

public class Host {

    public static final String LOCALHOST = "http://192.168.1.6/prediction_api/";

    public static String getCurrentHost() {
        return LOCALHOST;
    }
}
