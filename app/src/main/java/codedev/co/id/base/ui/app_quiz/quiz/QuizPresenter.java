package codedev.co.id.base.ui.app_quiz.quiz;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import com.google.gson.Gson;

import java.util.ArrayList;

import javax.inject.Inject;

import codedev.co.id.base.R;
import codedev.co.id.base.constant.Constant;
import codedev.co.id.base.constant.FragmentIndex;
import codedev.co.id.base.data.Quiz;
import codedev.co.id.base.data.QuizResult;
import codedev.co.id.base.ui.base.BasePresenter;
import codedev.co.id.base.utils.Extras;
import codedev.co.id.base.utils.StringHelper;

public class QuizPresenter extends BasePresenter<QuizView> {

    public ArrayList<Quiz.Data> mListQuestions;

    public QuizLevelType mQuizLevel;
    public Quiz.Data mCurrentQuestion;
    public int mCurrentCorrectAnswer;
    public int mQuestionIndex;

    public enum QuizLevelType {
        EASY, MEDIUM, HARD
    }

    @Inject
    public QuizPresenter() {
        mListQuestions = new ArrayList<>();
    }

    public void setupDataBundle(Bundle bundle) {
        if (bundle != null) {
            mQuizLevel = (QuizLevelType) bundle.get(Extras.QUIZ_LEVEL);
            mQuestionIndex = bundle.getInt(Extras.QUIZ_QUESTION_INDEX);
            //mCurrentQuestion = getQuestion();
            mCurrentCorrectAnswer = bundle.getInt(Extras.QUIZ_CORRECT_ANSWER);
        }
    }

    public void getQuiz(final Context context, QuizLevelType levelType) {
        if (isViewAttached()) {
            mMvpView.showLoading(Constant.LoadingType.SHOW);
            String filename = "";
            switch (levelType) {
                case EASY:
                    filename = "quiz.json";
                    break;
                case MEDIUM:
                    filename = "quiz2.json";
                    break;
                case HARD:
                    filename = "quiz3.json";
                    break;
            }
            final String json = StringHelper.loadJSONFromAsset(context, filename);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        Quiz quiz = new Gson().fromJson(json, Quiz.class);
                        mListQuestions = quiz.data;
                        mCurrentQuestion = getQuestion(); // setup current question
                        mMvpView.onSucceedGetContentQuiz();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        mMvpView.showLoading(Constant.LoadingType.HIDE);
                    }
                }
            }, 500);
        }
    }

    private Quiz.Data getQuestion() {
        if (mListQuestions != null) {
            return mListQuestions.get(mQuestionIndex);
        }
        return null;
    }

    public boolean isLastQuestion() {
        return mQuestionIndex == mListQuestions.size() - 1;
    }

    public double getLastPoint() {
        int point;
        if (mCurrentCorrectAnswer == mListQuestions.size()) {
            point = 100;
        } else {
            point = (100 / mListQuestions.size()) * mCurrentCorrectAnswer;
        }
        return point;
    }

    public boolean isCorrectAnswer(String answer) {
        if (answer.equalsIgnoreCase(mCurrentQuestion.answer)) {
            mCurrentCorrectAnswer++;
            return true;
        }
        return false;
    }

    public void processNextQuestion(Context context, String nextTitle) {
        Bundle bundle = new Bundle();
        if (!isLastQuestion()) {
            mQuestionIndex++;
            bundle.putInt(Extras.TITLE_ID, R.string.title_quiz);
            bundle.putString(Extras.QUIZ_TITLE_BAR, nextTitle);
            bundle.putInt(Extras.FRAGMENT_INDEX, FragmentIndex.FRAGMENT_QUIZ);
            bundle.putInt(Extras.QUIZ_QUESTION_INDEX, mQuestionIndex);
            bundle.putInt(Extras.QUIZ_CORRECT_ANSWER, mCurrentCorrectAnswer);
            bundle.putSerializable(Extras.QUIZ_LEVEL, mQuizLevel);
            mMvpView.onNextQuestionPage(bundle);
        } else {
            Double point = getLastPoint();
            String title = context.getString(R.string.resut_title_rejected);
            String description = context.getString(R.string.resut_description_rejected);
            if (point > 60) {
                title = context.getString(R.string.resut_title_completed);
                description = context.getString(R.string.resut_description_completed);
            }

            QuizResult result = new QuizResult();
            result.point = point;
            result.title = title;
            result.description = description;
            result.quizLevelType = mQuizLevel;

            bundle.putInt(Extras.TITLE_ID, R.string.title_quiz);
            bundle.putInt(Extras.FRAGMENT_INDEX, FragmentIndex.FRAGMENT_QUIZ_RESULT);
            bundle.putParcelable(Extras.QUIZ_RESULT, result);
            mMvpView.onNextResultPage(bundle);
        }
    }

}
