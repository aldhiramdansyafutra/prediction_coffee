package codedev.co.id.base.ui.app_prediction.home;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;

import javax.inject.Inject;

import butterknife.BindView;
import codedev.co.id.base.R;
import codedev.co.id.base.data.model_prediction.PopularPrediction;
import codedev.co.id.base.ui.base.BaseFragment;

public class HomeFragment extends BaseFragment implements HomeMvpView {

    @BindView(R.id.bar_chart_product)
    BarChart mHorizontalBarChart;

    @Inject
    HomePresenter mPresenter;

    @Override
    protected void attachViewForPresenter() {
        mPresenter.attachView(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_prediction_home;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        chartInitialize();
        mPresenter.getPopularProduct("2018");
    }

    @Override
    public void showProgressBarLoading() {
        showLoading();
    }

    @Override
    public void onFailedRequest() {
        dissmissLoading();
    }

    private void chartInitialize() {
        mHorizontalBarChart.setDescription("Calculate by Sold of Product");
        mHorizontalBarChart.animateXY(2000, 2000);
        mHorizontalBarChart.invalidate();
    }

    @Override
    public void onSucceedGetProduct(PopularPrediction.Result results) {
        dissmissLoading();
        mHorizontalBarChart.setData(mPresenter.getBarData());
        mHorizontalBarChart.invalidate();
    }
}
