package codedev.co.id.base.ui.app_prediction;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import butterknife.BindView;
import codedev.co.id.base.R;
import codedev.co.id.base.ui.app_prediction.home.HomeActivity;
import codedev.co.id.base.ui.base.BaseActivity;

public class SplashScreenActivity extends BaseActivity {
    @BindView(R.id.iv_logo)
    ImageView ivLogoSplash;

    @Override
    protected int getContentView() {
        return R.layout.activity_app_location_splash;
    }

    @Override
    protected int getTitleBar() {
        return R.string.location_title;
    }

    @Override
    protected int getToolbarColorId() {
        return R.color.colorPrimaryDark;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_in_up);
        animation.setInterpolator(new LinearInterpolator());
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        navigateActivity(SplashScreenActivity.this, new Bundle(), R.string.location_title, HomeActivity.class);
                        animationOpenActivity();
                        finish();
                    }
                }, 1000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        ivLogoSplash.startAnimation(animation);
    }
}
