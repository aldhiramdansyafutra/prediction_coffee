package codedev.co.id.base.ui.app_location.list;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import codedev.co.id.base.R;
import codedev.co.id.base.data.Location;
import codedev.co.id.base.utils.Tools;

public class SchoolLocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private ArrayList<Location.Data> mItems;
    private ArrayList<Location.Data> mFilteredItems;
    private Context mContext;

    @LayoutRes
    private int contentView;

    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, Location.Data obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public SchoolLocationAdapter(Context mContext, @LayoutRes int contentView) {
        mItems = new ArrayList<>();
        this.mContext = mContext;
        this.contentView = contentView;
        this.mFilteredItems = new ArrayList<>();
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView title;
        public TextView description;
        public TextView phone;
        public View parentLayout;

        public OriginalViewHolder(View v) {
            super(v);
            image = v.findViewById(R.id.image);
            title = v.findViewById(R.id.title);
            description = v.findViewById(R.id.description);
            phone = v.findViewById(R.id.phone);
            parentLayout = v.findViewById(R.id.lyt_parent);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(contentView, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            Location.Data item = mFilteredItems.get(position);
            view.title.setText(item.title);
            view.description.setText(item.information);
            view.phone.setText(item.phone);
            Tools.displayImageOriginal(mContext, view.image, item.image);
            view.parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener == null) return;
                    mOnItemClickListener.onItemClick(view, mFilteredItems.get(position), position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredItems.size();
    }

    public void addData(ArrayList<Location.Data> result) {
        if (result != null) {
            mItems.clear();
            mItems.addAll(result);
            mFilteredItems = mItems;
        }
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                try {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        mFilteredItems = mItems;
                    } else {
                        ArrayList<Location.Data> itemFiltered = new ArrayList<>();
                        for (Location.Data item : mItems) {
                            if (item.title.toLowerCase().contains(charString.toLowerCase())) {
                                itemFiltered.add(item);
                            }
                        }
                        mFilteredItems = itemFiltered;
                    }
                    filterResults.values = mFilteredItems;
                    return filterResults;
                } catch (Exception e) {
                    e.printStackTrace();
                    return filterResults;
                }
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredItems = (ArrayList<Location.Data>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}