package codedev.co.id.base.ui.app_location.list.detail;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.data.Location;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.Extras;

import static android.app.Activity.RESULT_OK;

public class SchoolListLocationDetailFragment extends BaseFragment {

    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.tv_description)
    TextView mTvDescription;
    @BindView(R.id.tv_address)
    TextView mTvAddress;
    @BindView(R.id.tv_phone)
    TextView mTvPhoneNumber;

    View mViewRoot;
    int REQUEST_PHONE_CALL = 101;

    @Override
    protected void attachViewForPresenter() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mViewRoot = super.onCreateView(inflater, container, savedInstanceState);
        return mViewRoot;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            try {
                final Location.Data location = getArguments().getParcelable(Extras.DETAIL_LOCATION);
                mTvTitle.setText(location.title);
                mTvDescription.setText(location.information);
                mTvAddress.setText(location.address);
                mTvPhoneNumber.setText(location.phone);
                getActivity().findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startDirection(location.latitude, location.longitude);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_app_location_detail;
    }

    @OnClick(R.id.tv_phone)
    void handleCall() {
        checkPermission(mTvPhoneNumber.getText().toString());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PHONE_CALL && resultCode == RESULT_OK) {
            phoneCall(mTvPhoneNumber.getText().toString());
        }
    }

    private void startDirection(double destLatitude, double destLongitude) {
        String uri = "http://maps.google.com/maps?daddr=" + destLatitude + "," + destLongitude;
        Intent directionIntent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(directionIntent);
    }

    private void phoneCall(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone));
        startActivity(callIntent);
    }

    private void checkPermission(String phone) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
        } else {
            phoneCall(phone);
        }
    }
}
