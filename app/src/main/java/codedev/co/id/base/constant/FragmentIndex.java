package codedev.co.id.base.constant;

public final class FragmentIndex {

    public static final int FRAGMENT_QUIZ = 0;
    public static final int FRAGMENT_QUIZ_RESULT = 1;
}
