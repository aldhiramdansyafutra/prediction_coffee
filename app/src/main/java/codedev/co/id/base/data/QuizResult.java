package codedev.co.id.base.data;

import android.os.Parcel;
import android.os.Parcelable;

import codedev.co.id.base.ui.app_quiz.quiz.QuizPresenter;

public class QuizResult implements Parcelable {

    public String title;
    public String description;
    public double point;
    public QuizPresenter.QuizLevelType quizLevelType;

    public QuizResult() {
        // Empty constructor
    }

    protected QuizResult(Parcel in) {
        title = in.readString();
        description = in.readString();
        point = in.readDouble();
        quizLevelType = (QuizPresenter.QuizLevelType) in.readSerializable();
    }

    public static final Creator<QuizResult> CREATOR = new Creator<QuizResult>() {
        @Override
        public QuizResult createFromParcel(Parcel in) {
            return new QuizResult(in);
        }

        @Override
        public QuizResult[] newArray(int size) {
            return new QuizResult[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeDouble(point);
        dest.writeSerializable(quizLevelType);
    }
}
