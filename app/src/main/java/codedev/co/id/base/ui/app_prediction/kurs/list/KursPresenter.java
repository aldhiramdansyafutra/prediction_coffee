package codedev.co.id.base.ui.app_prediction.kurs.list;

import javax.inject.Inject;

import codedev.co.id.base.data.DataManager;
import codedev.co.id.base.data.model_prediction.Kurs;
import codedev.co.id.base.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KursPresenter extends BasePresenter<KursMvpView> {

    @Inject
    public KursPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    public void getData() {
        if (isViewAttached()) {
            mMvpView.showProgressBarLoading();
            mDataManager.getKurs().enqueue(new Callback<Kurs.Result>() {
                @Override
                public void onResponse(Call<Kurs.Result> call, Response<Kurs.Result> response) {
                    Kurs.Result result = response.body();
                    if (result != null) {
                        mMvpView.onSucceed(result.products);
                    } else {
                        mMvpView.onFailedRequest();
                    }
                }

                @Override
                public void onFailure(Call<Kurs.Result> call, Throwable t) {
                    mMvpView.onFailedRequest();
                }
            });
        }
    }
}
