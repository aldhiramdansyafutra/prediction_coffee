package codedev.co.id.base.ui.app_prediction.kurs.form;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;

import javax.inject.Inject;

import butterknife.BindView;
import codedev.co.id.base.R;
import codedev.co.id.base.data.model_prediction.Kurs;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.Extras;

public class KursFormFragment extends BaseFragment implements KursFormMvpView {

    @BindView(R.id.til_id)
    TextInputLayout tilKursId;
    @BindView(R.id.til_kurs_value)
    TextInputLayout tilKursValue;
    @BindView(R.id.til_month)
    TextInputLayout tilMonth;
    @BindView(R.id.til_year)
    TextInputLayout tilYear;

    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.btn_search)
    Button btnSearch;
    @BindView(R.id.btn_generate_id)
    Button btnGenerateId;

    @Inject
    KursFormPresenter mPresenter;

    boolean isFormDetail;

    @Override
    protected void attachViewForPresenter() {
        mPresenter.attachView(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_prediction_form_kurs;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupView(getArguments());
    }

    private void setupView(Bundle bundle) {
        if (bundle != null && bundle.containsKey(Extras.KURS_DETAIL)) {
            Kurs kurs = bundle.getParcelable(Extras.KURS_DETAIL);
            isFormDetail = kurs != null;

            btnSubmit.setText("Submit");
            btnSearch.setVisibility(View.VISIBLE);
            btnGenerateId.setVisibility(View.VISIBLE);

            if (kurs != null) {
                tilKursId.clearFocus();
                tilKursId.getEditText().setFocusable(false);
                tilKursId.getEditText().setText(kurs.kursId);
                tilKursValue.requestFocus();
                tilKursValue.getEditText().setText(String.valueOf(kurs.nominal));
                tilMonth.getEditText().setText(String.valueOf(kurs.month));
                tilYear.getEditText().setText(kurs.year);

                btnSubmit.setText("Update");
                btnSearch.setVisibility(View.GONE);
                btnGenerateId.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onSucceedSubmitData() {
        dissmissLoading();
        getActivity().finish();
        animationCloseActivity();
    }

    @Override
    public void onSuceedUpdateData() {
        dissmissLoading();
        getActivity().finish();
        animationCloseActivity();
    }

    @Override
    public void showProgressBarLoading() {
        showLoading();
    }

    @Override
    public void onFailedRequest() {
        dissmissLoading();
    }
}
