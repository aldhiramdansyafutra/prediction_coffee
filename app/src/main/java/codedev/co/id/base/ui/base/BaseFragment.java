package codedev.co.id.base.ui.base;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatDelegate;
import android.text.Selection;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.luseen.simplepermission.permissions.PermissionFragment;

import butterknife.ButterKnife;
import codedev.co.id.base.BaseApplication;
import codedev.co.id.base.R;
import codedev.co.id.base.constant.Constant;
import codedev.co.id.base.injection.component.DaggerFragmentComponent;
import codedev.co.id.base.injection.component.FragmentComponent;
import codedev.co.id.base.injection.module.FragmentModule;
import codedev.co.id.base.ui.app_quiz.quiz.QuizActivity;
import codedev.co.id.base.utils.Config;
import codedev.co.id.base.utils.RequestCodes;
import codedev.co.id.base.utils.StringHelper;
import codedev.co.id.base.widget.ViewLoadingDotsGrow;

public abstract class BaseFragment extends PermissionFragment {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public RelativeLayout mProgressBar;
    private FragmentComponent mFragmentComponent;
    private CustomTabsIntent customTabsIntent;
    private CustomTabsIntent.Builder mCustomTabIntentBuilder;

    public static Fragment newInstanceFragment(Bundle bundle, Fragment fragment) {
        if (fragment != null && bundle != null) {
            fragment.setArguments(bundle);
        }

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCustomTabIntentBuilder = new CustomTabsIntent.Builder();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, viewGroup);
        attachViewForPresenter();
        fragmentComponent().inject(this);
        buildChromeTabs();
        return viewGroup;
    }

    protected abstract void attachViewForPresenter();

    public FragmentComponent fragmentComponent() {
        if (mFragmentComponent == null) {
            mFragmentComponent = DaggerFragmentComponent.builder().fragmentModule(new FragmentModule(this)).applicationComponent(BaseApplication.get(getActivity()).getComponent()).build();
        }
        return mFragmentComponent;
    }

    private void buildChromeTabs() {
        customTabsIntent = mCustomTabIntentBuilder.build();
        try {
            customTabsIntent.intent.setPackage("com.android.chrome");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void animationOpenActivity() {
        getActivity().overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    public void animationCloseActivity() {
        getActivity().overridePendingTransition(R.anim.anim_slide_out_right, R.anim.anim_slide_in_right);
    }

    public void animationOpenUpActivity() {
        getActivity().overridePendingTransition(R.anim.anim_slide_up, R.anim.stay);
    }

    public void animationCloseDownAcitivity() {
        getActivity().overridePendingTransition(R.anim.stay, R.anim.anim_slide_down);
    }

    protected abstract int getLayout();

    protected void goToApplicationSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + getActivity().getPackageName()));
        startActivityForResult(intent, RequestCodes.GO_TO_SETTINGS);
        animationOpenActivity();
    }

    protected void goToWeb(String url) {
        if (StringHelper.validUrlFormat(url)) {
            if (new Intent(Intent.ACTION_VIEW, Uri.parse(url)).resolveActivity(getActivity().getPackageManager()) != null) {
                customTabsIntent.launchUrl(getContext(), Uri.parse(url));
                animationOpenActivity();
            } else {
                Toast.makeText(getContext(), "Browser tidak ditemukan", Toast.LENGTH_SHORT).show();
            }
        }
    }

    protected void setTogglePassword(String input, TextInputLayout til) {
        if (input.length() == 0) {
            til.setPasswordVisibilityToggleEnabled(false);
        } else {
            til.setPasswordVisibilityToggleEnabled(true);
        }
    }

    protected void hideEditTextCursor(EditText editText) {
        editText.setFocusable(false);

        // Set EditText to be focusable again
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
    }

    protected void hideErrorEditText(TextInputLayout... tils) {
        for (TextInputLayout til : tils) {
            til.setError(null);
            til.setErrorEnabled(false);
        }
    }

    public void setDefaultPhoneFormat(EditText edittext) {
        edittext.setText(Config.INDONESIA_PHONE_CODE);
        Selection.setSelection(edittext.getText(), edittext.getText().length());
    }

    public void removeDefaultPhoneFormat(EditText editText) {
        Selection.removeSelection(editText.getText());
    }

    public void goSendEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{Config.EMAIL_CONTACT_US});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Permohonan penggantian profile");
        startActivity(Intent.createChooser(intent, ""));
    }

    public void setDataEditText(TextInputLayout til, String value) {
        EditText editText = til.getEditText();
        if (editText != null) {
            editText.setText(value);
        }
    }

    public void scrollToSpesificView(final ScrollView scrollView, final ViewGroup viewGroup) {
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollTo(0, viewGroup.getTop());
            }
        });
    }

    public void scrollToSpesificView(final NestedScrollView scrollView, final ViewGroup viewGroup) {
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollTo(0, viewGroup.getTop());
            }
        });
    }

    public void finishAllActivityAndGoToRootHomeScreen() {
        BaseActivity baseActivity = (BaseActivity) getActivity();
        baseActivity.finishAllActivityAndGoToRootHomeScreen();
    }

    public void finishQuizAndGoToRootHomeScreen() {
        BaseActivity baseActivity = (BaseActivity) getActivity();
        baseActivity.finishQuizAndGoToRootHomeScreen();
    }

    public void handlingPrgogressbar(Constant.LoadingType loadingType) {
        switch (loadingType) {
            case ERROR:
                dissmissLoading();
                break;
            case HIDE:
                dissmissLoading();
                break;
            case SHOW:
                showLoading();
                break;
        }
    }

    public void showLoading() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    public void dissmissLoading() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    public void goToQuizActivity(Bundle bundle) {
        startActivity(BaseActivity.getStartIntent(getActivity(), bundle, QuizActivity.class));
        animationOpenActivity();
    }
}
