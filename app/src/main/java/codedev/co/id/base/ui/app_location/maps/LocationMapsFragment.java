package codedev.co.id.base.ui.app_location.maps;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.constant.Constant;
import codedev.co.id.base.ui.base.BaseFragment;

public class LocationMapsFragment extends BaseFragment implements OnMapReadyCallback, LocationMapsView, GoogleMap.OnMarkerClickListener {

    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.tv_address)
    TextView mTvAddress;
    @BindView(R.id.tv_phone)
    TextView mTvPhone;
    @BindView(R.id.fab_directions)
    FloatingActionButton mFabDirection;
    @BindView(R.id.bottom_sheet)
    LinearLayout mLayoutBottomSheet;

    View mRootView;
    SupportMapFragment mSupportMapFragment;
    BottomSheetBehavior bottomSheetBehavior;

    @Inject
    LocationsMapsPresenter mPresenter;

    @Override
    protected void attachViewForPresenter() {
        mPresenter.attachView(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_app_location_maps;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = super.onCreateView(inflater, container, savedInstanceState);
        return mRootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressBar = ButterKnife.findById(mRootView, R.id.layout_progress);
        mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSupportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        initBottomSheet();
        mPresenter.getLocation(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-6.389593, 106.816439), 11));
        //googleMap.setPadding(0, 100, 0, 0);
        //googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.setOnMarkerClickListener(this);
        mPresenter.loadMarkers(googleMap);
    }

    @Override
    public void onSucceedGetLocation() {
        mSupportMapFragment.getMapAsync(this);
    }

    @Override
    public void showLoading(Constant.LoadingType type) {
        handlingPrgogressbar(type);
    }

    @Override
    public void onError(String errMessage) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        mPresenter.currentLocation = mPresenter.getLocationDetail(marker.getPosition().latitude);
        if (mPresenter.currentLocation != null) {
            mTvTitle.setText(mPresenter.currentLocation.title);
            mTvAddress.setText(mPresenter.currentLocation.address);
            mTvPhone.setText(mPresenter.currentLocation.phone);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            mFabDirection.show();
        }
        return false;
    }

    @OnClick({R.id.tv_title, R.id.iv_swipe, R.id.fab_directions})
    public void handleOnClick(View view) {
        switch (view.getId()) {
            case R.id.tv_title:
            case R.id.iv_swipe:
                //mMap.animateCamera(zoomingLocation());
                handleFloating();
                break;
            case R.id.fab_directions:
                if (mPresenter.currentLocation != null)
                    startDirection(mPresenter.currentLocation.latitude, mPresenter.currentLocation.longitude);
                break;
        }
    }

    private void initBottomSheet() {
        mFabDirection.hide();
        bottomSheetBehavior = BottomSheetBehavior.from(mLayoutBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void startDirection(double destLatitude, double destLongitude) {
        String uri = "http://maps.google.com/maps?daddr=" + destLatitude + "," + destLongitude;
        Intent directionIntent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(directionIntent);
    }

    private void handleFloating() {
        if (mPresenter.currentLocation != null) {
            if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                mFabDirection.show();
            } else if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                mFabDirection.hide();
            }
        }
    }
}
