package codedev.co.id.base.injection.module;

import android.app.Application;
import android.content.Context;
import android.support.customtabs.CustomTabsIntent;

import com.google.android.gms.common.GoogleApiAvailability;

import javax.inject.Singleton;

import codedev.co.id.base.data.network.ApiServices;
import codedev.co.id.base.injection.ApplicationContext;
import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    GoogleApiAvailability provideGoogleApiAvailability() {
        return GoogleApiAvailability.getInstance();
    }

    @Provides
    @Singleton
    ApiServices provideApiService() {
        return ApiServices.Factory.makeApiService(mApplication);
    }
}
