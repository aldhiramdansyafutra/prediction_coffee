package codedev.co.id.base.ui.app_prediction.product.form;

import android.support.design.widget.TextInputLayout;

import javax.inject.Inject;

import codedev.co.id.base.data.DataManager;
import codedev.co.id.base.data.model_prediction.BaseResponse;
import codedev.co.id.base.data.model_prediction.GenerateId;
import codedev.co.id.base.data.model_prediction.Product;
import codedev.co.id.base.ui.base.BasePresenter;
import codedev.co.id.base.utils.StringHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductFormPresenter extends BasePresenter<ProductFormMvpView> {
    Product mProduct;
    String generatedId;

    @Inject
    public ProductFormPresenter(DataManager dataManager) {
        mDataManager = dataManager;
        mProduct = new Product();
    }

    public void submitProduct() {
        if (isViewAttached()) {
            mMvpView.showProgressBarLoading();
            mDataManager.createProduct(mProduct).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.body() != null) {
                        mMvpView.onSucceedCreateProduct();
                    } else {
                        mMvpView.onFailedRequest();
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    mMvpView.onFailedRequest();
                }
            });
        }
    }

    public void updateProduct() {
        if (isViewAttached()) {
            mMvpView.showProgressBarLoading();
            if (mProduct.id != 0) {
                mDataManager.updateProduct(mProduct).enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.body() != null) {
                            mMvpView.onSucceedUpdateProduct();
                        } else {
                            mMvpView.onFailedRequest();
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        mMvpView.onFailedRequest();
                    }
                });
            }
        }
    }

    public void deleteProduct(int id) {
        if (isViewAttached()) {
            mMvpView.showProgressBarLoading();
            mDataManager.deleteProduct(id).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {

                }
            });
        }
    }

    public void getProduct(String productId) {
        if (isViewAttached()) {
            mMvpView.showProgressBarLoading();
            mDataManager.getProduct(productId).enqueue(new Callback<Product.Result>() {
                @Override
                public void onResponse(Call<Product.Result> call, Response<Product.Result> response) {
                    if (response.body() != null && response.body().products != null) {
                        Product.Result result = response.body();
                        if (result.products.size() > 0) {
                            mMvpView.onSucceedGetProduct(result.products.get(0));
                        } else  {
                            mMvpView.onFailedRequest();
                        }
                    } else {
                        mMvpView.onFailedRequest();
                    }
                }

                @Override
                public void onFailure(Call<Product.Result> call, Throwable t) {
                    mMvpView.onFailedRequest();
                }
            });
        }
    }

    public void generateId() {
        if (isViewAttached()) {
            mMvpView.showProgressBarLoading();
            mDataManager.generateProductId().enqueue(new Callback<GenerateId>() {
                @Override
                public void onResponse(Call<GenerateId> call, Response<GenerateId> response) {
                    if (response.body() != null && StringHelper.isNotEmpty(response.body().generatedId)) {
                        generatedId = response.body().generatedId;
                        mMvpView.onSucceedGenerateId();
                    } else {
                        mMvpView.onFailedRequest();
                    }
                }

                @Override
                public void onFailure(Call<GenerateId> call, Throwable t) {
                    mMvpView.onFailedRequest();
                }
            });
        }
    }

    public boolean validate(TextInputLayout tilProductId,
                            TextInputLayout tilName,
                            TextInputLayout tilPrice,
                            TextInputLayout tilKurs,
                            TextInputLayout tilMonth,
                            TextInputLayout tilYear) {
        boolean isValid = isFormValidate(tilProductId, tilName, tilPrice, tilKurs, tilMonth, tilYear);
        if (isValid) {
            mProduct.productId = StringHelper.getStringFromEditText(tilProductId.getEditText());
            mProduct.name = StringHelper.getStringFromEditText(tilName.getEditText());
            mProduct.price = Integer.valueOf(StringHelper.getStringFromEditText(tilPrice.getEditText()));
            mProduct.soldItem = Integer.valueOf(StringHelper.getStringFromEditText(tilKurs.getEditText()));
            mProduct.month = Integer.valueOf(StringHelper.getStringFromEditText(tilMonth.getEditText()));
            mProduct.year = StringHelper.getStringFromEditText(tilYear.getEditText());
        }
        return isValid;
    }

    public boolean isFormValidate(TextInputLayout... inputLayouts) {
        boolean isValid = true;
        for (TextInputLayout input : inputLayouts) {
            String value = input.getEditText().getText().toString();
            if (StringHelper.isEmpty(value)) {
                input.setError("Data wajib di isi");
                isValid = false;
            }
        }
        return isValid;
    }
}
