package codedev.co.id.base.ui.banner;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import codedev.co.id.base.R;
import codedev.co.id.base.data.Banner;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.Extras;

public class BannerItemFragment extends BaseFragment {

    public static Fragment newInstance(Banner banner, int position) {
        Bundle args = new Bundle();
        args.putParcelable(Extras.BANNER, banner);
        args.putInt(Extras.BANNER_TYPE_ICON, position);
        return BaseFragment.newInstanceFragment(args, new BannerItemFragment());
    }

    @BindView(R.id.iv_icon)
    ImageView mIconBanner;
    @BindView(R.id.tv_title)
    TextView mTextTitle;
    @BindView(R.id.tv_description)
    TextView mTextDescription;

    Context mContext;
    Banner mBanner;
    TypedArray mIconList;
    int mItemPosition;

    @Override
    protected int getLayout() {
        return R.layout.item_banner;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(Extras.BANNER)) {
            mBanner = getArguments().getParcelable(Extras.BANNER);
        }
        if (getArguments() != null && getArguments().containsKey(Extras.BANNER_TYPE_ICON)) {
            mItemPosition = getArguments().getInt(Extras.BANNER_TYPE_ICON);
        }
        //mIconList = getResources().obtainTypedArray(R.array.banner_icon);
    }

    @Override
    protected void attachViewForPresenter() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mBanner != null) {
            mTextTitle.setText(mBanner.title);
            mTextDescription.setText(mBanner.description);
            mIconBanner.setImageDrawable(ContextCompat.getDrawable(mContext, mIconList.getResourceId(mItemPosition, 0)));
        }
    }
}
