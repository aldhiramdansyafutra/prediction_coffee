package codedev.co.id.base.ui.app_location.maps;

import android.content.Context;
import android.os.Handler;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import codedev.co.id.base.constant.Constant;
import codedev.co.id.base.data.Location;
import codedev.co.id.base.ui.base.BasePresenter;
import codedev.co.id.base.utils.StringHelper;

public class LocationsMapsPresenter extends BasePresenter<LocationMapsView> {

    public List<Location.Data> mLocationList;
    public HashMap<Double, Integer> mMarkers;
    public Location.Data currentLocation;

    @Inject
    public LocationsMapsPresenter() {
        mLocationList = new ArrayList<>();
        mMarkers = new HashMap<>();
    }

    public void getLocation(final Context context) {
        if (isViewAttached()) {
            mMvpView.showLoading(Constant.LoadingType.SHOW);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        String json = StringHelper.loadJSONFromAsset(context, "location.json");
                        Location locationResult = new Gson().fromJson(json, Location.class);
                        mLocationList = locationResult.list;
                        mMvpView.onSucceedGetLocation();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        mMvpView.showLoading(Constant.LoadingType.HIDE);
                    }
                }
            }, 3000);
        }
    }

    public void loadMarkers(GoogleMap googleMap) {
        int count = mLocationList.size();
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                final Location.Data item = mLocationList.get(i);
                googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(item.latitude, item.longitude))
                        .title(item.title)
                        .icon(BitmapDescriptorFactory.defaultMarker()));
                mMarkers.put(item.latitude, item.id);
            }
        }
    }

    public Location.Data getLocationDetail(double latitude) {
        for (Location.Data item : mLocationList) {
            if (item.latitude == latitude) {
                return item;
            }
        }
        return null;
    }
}
