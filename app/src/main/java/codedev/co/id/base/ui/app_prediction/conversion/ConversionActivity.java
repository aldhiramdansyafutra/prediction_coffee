package codedev.co.id.base.ui.app_prediction.conversion;

import android.os.Bundle;
import android.support.annotation.Nullable;

import codedev.co.id.base.ui.base.BaseActivityWithToolbar;

public class ConversionActivity extends BaseActivityWithToolbar {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupFragment(getIntent().getExtras());
    }

    private void setupFragment(Bundle bundle) {
        ConversionFragment fragment = new ConversionFragment();
        fragment.setArguments(bundle);
        setupDefaultFragment(fragment);
    }
}
