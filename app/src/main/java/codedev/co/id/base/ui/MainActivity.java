package codedev.co.id.base.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import codedev.co.id.base.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
