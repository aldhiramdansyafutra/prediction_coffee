package codedev.co.id.base.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Location implements Parcelable {

    @SerializedName("data")
    @Expose
    public ArrayList<Data> list;

    protected Location(Parcel in) {
    }

    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel in) {
            return new Location(in);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class Data implements Parcelable{

        @SerializedName("id")
        public int id;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("phone")
        @Expose
        public String phone;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("information")
        @Expose
        public String information;
        @SerializedName("latitude")
        @Expose
        public double latitude;
        @SerializedName("longitude")
        @Expose
        public double longitude;
        @SerializedName("sound")
        @Expose
        public String sound;

        protected Data(Parcel in) {
            id = in.readInt();
            title = in.readString();
            address = in.readString();
            phone = in.readString();
            image = in.readString();
            information = in.readString();
            latitude = in.readDouble();
            longitude = in.readDouble();
            sound = in.readString();
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(title);
            dest.writeString(address);
            dest.writeString(phone);
            dest.writeString(image);
            dest.writeString(information);
            dest.writeDouble(latitude);
            dest.writeDouble(longitude);
            dest.writeString(sound);
        }
    }
}
