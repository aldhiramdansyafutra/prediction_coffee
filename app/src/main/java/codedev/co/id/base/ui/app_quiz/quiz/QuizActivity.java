package codedev.co.id.base.ui.app_quiz.quiz;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import codedev.co.id.base.R;
import codedev.co.id.base.constant.FragmentIndex;
import codedev.co.id.base.ui.base.BaseActivityWithToolbar;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.Extras;

public class QuizActivity extends BaseActivityWithToolbar {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupFragment();
    }

    private void setupFragment() {
        Bundle bundle;
        Fragment fragment = null;
        if (getIntent().getExtras() != null) {
            bundle = getIntent().getExtras();
            if (bundle.containsKey(Extras.FRAGMENT_INDEX)) {
                int index = bundle.getInt(Extras.FRAGMENT_INDEX);
                switch (index) {
                    case FragmentIndex.FRAGMENT_QUIZ:
                        fragment = BaseFragment.newInstanceFragment(bundle, new QuizFragment());
                        break;
                    case FragmentIndex.FRAGMENT_QUIZ_RESULT:
                        fragment = BaseFragment.newInstanceFragment(bundle, new QuizResultFragment());
                        break;
                }
            }

        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment, fragment.getTag()).commit();
    }
}
