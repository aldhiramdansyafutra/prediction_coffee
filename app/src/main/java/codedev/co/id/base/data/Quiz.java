package codedev.co.id.base.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Quiz {

    @SerializedName("data")
    @Expose
    public ArrayList<Data> data;

    public static class Data implements Parcelable {

        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("answer")
        @Expose
        public String answer;
        @SerializedName("question")
        @Expose
        public String question;
        @SerializedName("image")
        @Expose
        public String image;
        @SerializedName("answer_1")
        @Expose
        public String answer1;
        @SerializedName("answer_2")
        @Expose
        public String answer2;
        @SerializedName("answer_3")
        @Expose
        public String answer3;
        @SerializedName("answer_4")
        @Expose
        public String answer4;

        protected Data(Parcel in) {
            id = in.readInt();
            answer = in.readString();
            question = in.readString();
            image = in.readString();
            answer1 = in.readString();
            answer2 = in.readString();
            answer3 = in.readString();
            answer4 = in.readString();
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(answer);
            dest.writeString(question);
            dest.writeString(image);
            dest.writeString(answer1);
            dest.writeString(answer2);
            dest.writeString(answer3);
            dest.writeString(answer4);
        }
    }
}
