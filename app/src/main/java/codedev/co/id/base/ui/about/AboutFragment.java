package codedev.co.id.base.ui.about;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import codedev.co.id.base.R;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.Extras;

public class AboutFragment extends BaseFragment {

    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.tv_version)
    TextView mTvVersion;
    @BindView(R.id.tv_last_update)
    TextView mTvLastUpdate;
    @BindView(R.id.tv_description)
    TextView mTvDescription;

    @Override
    protected void attachViewForPresenter() {

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_about;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mTvTitle.setText(bundle.getString(Extras.ABOUT_TITLE));
            mTvVersion.setText(bundle.getString(Extras.ABOUT_VERSION));
            mTvLastUpdate.setText(bundle.getString(Extras.ABOUT_LAST_UPDATE));
            mTvDescription.setText(bundle.getString(Extras.ABOUT_DESCRIPTION));
        }
    }
}
