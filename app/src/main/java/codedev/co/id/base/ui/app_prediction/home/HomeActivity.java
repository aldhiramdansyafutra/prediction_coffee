package codedev.co.id.base.ui.app_prediction.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import codedev.co.id.base.R;
import codedev.co.id.base.ui.app_prediction.conversion.ConversionActivity;
import codedev.co.id.base.ui.app_prediction.product.form.ProductFormActivity;
import codedev.co.id.base.ui.app_prediction.product.list.ProductActivity;
import codedev.co.id.base.utils.Extras;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        setupDefaultFragment(new HomeFragment());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent = null;

        if (id == R.id.nav_product_list) {
            intent = new Intent(HomeActivity.this, ProductActivity.class);
            intent.putExtra(Extras.TITLE_ID, R.string.product_title);
        } else if (id == R.id.nav_conversion) {
            intent = new Intent(HomeActivity.this, ConversionActivity.class);
            intent.putExtra(Extras.TITLE_ID, R.string.prediction_title);
        } else if (id == R.id.nav_new_product) {
            intent = new Intent(HomeActivity.this, ProductFormActivity.class);
            intent.putExtra(Extras.TITLE_ID, R.string.product_new);
        }

        if (intent != null) startActivity(intent);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setupDefaultFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_main, fragment, fragment.getTag()).commit();
    }
}
