package codedev.co.id.base.ui.app_quiz.quiz;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.constant.Constant;
import codedev.co.id.base.data.Quiz;
import codedev.co.id.base.interfaces.DialogButtonListener;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.DialogFactory;

public class QuizFragment extends BaseFragment implements QuizView {

    @BindView(R.id.tv_question)
    TextView mTextQuestion;
    @BindView(R.id.radio_group)
    RadioGroup mRadioGroup;
    @BindView(R.id.btn_answer_a)
    RadioButton mButtonAnswer1;
    @BindView(R.id.btn_answer_b)
    RadioButton mButtonAnswer2;
    @BindView(R.id.btn_answer_c)
    RadioButton mButtonAnswer3;
    @BindView(R.id.btn_answer_d)
    RadioButton mButtonAnswer4;

    @Inject
    QuizPresenter mPresenter;

    View mRootView;

    @Override
    protected void attachViewForPresenter() {
        mPresenter.attachView(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_quiz;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = super.onCreateView(inflater, container, savedInstanceState);
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.setupDataBundle(getArguments());
        mPresenter.getQuiz(getActivity(), mPresenter.mQuizLevel);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @OnClick(R.id.btn_submit)
    public void handleSubmit(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                checkQuestion();
                break;
        }
    }


    @Override
    public void showLoading(Constant.LoadingType type) {
        handlingPrgogressbar(type);
    }

    @Override
    public void onError(String errMessage) {

    }

    @Override
    public void onSucceedGetContentQuiz() {
        setupQuestionView();
        mRadioGroup.clearCheck();
    }

    @Override
    public void onNextQuestionPage(Bundle bundle) {
        goToQuizActivity(bundle);
        getActivity().finish();
    }

    @Override
    public void onNextResultPage(Bundle bundle) {
        goToQuizActivity(bundle);
        getActivity().finish();
    }

    private void setupQuestionView() {
        Quiz.Data question = mPresenter.mCurrentQuestion;
        mTextQuestion.setText(question.question);
        mButtonAnswer1.setText(question.answer1);
        mButtonAnswer2.setText(question.answer2);
        mButtonAnswer3.setText(question.answer3);
        mButtonAnswer4.setText(question.answer4);
    }

    private void checkQuestion() {
        String title;
        String message;
        RadioButton btn = mRadioGroup.findViewById(mRadioGroup.getCheckedRadioButtonId());
        if (btn != null) {
            boolean isCorrectAnswer = mPresenter.isCorrectAnswer(btn.getText().toString());
            if (!mPresenter.isLastQuestion()) {
                if (isCorrectAnswer) {
                    title = "Hebaatt!";
                    message = "Jawaban kamu benar, silahkan lanjutkan ke pertanyaan berikutnya :)";
                } else {
                    title = "Oops, Maaf :(";
                    message = "Jawaban kamu kurang tepat, ayoo lebih teliti lagi untuk menjawab soal berikutnya :)";
                }
            } else {
                title = "Selamat!";
                message = "Kamu sudah berada di akhir pertanyaan sesi quiz ini, yuk lihat hasil perolehan point kamu :)";
            }
            DialogFactory.createDialog(getActivity(), title, message, "LANJUT", "", dialogButtonListener).show();
        } else {
            DialogFactory.createDialog(getActivity(), "Oops...", "Kamu belum memilih jawaban. Pilih jawaban kamu terlebih dahulu yaa...", "", "OKE, SAYA MENGERTI", null).show();
        }
    }

    private DialogButtonListener dialogButtonListener = new DialogButtonListener() {
        @Override
        public void onPositiveDialogButtonListener() {
            String title = getString(R.string.title_quiz_ongoing, "" + (mPresenter.mQuestionIndex + 2));
            mPresenter.processNextQuestion(getActivity(), title);
        }

        @Override
        public void onNegativeDialogButtonListener() {

        }
    };
}
