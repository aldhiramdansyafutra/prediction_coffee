package codedev.co.id.base.interfaces;

public interface HandlingCollapsingImageToolbarListener {
    void doExpanded(boolean isExpanded);
}
