package codedev.co.id.base.injection.component;

import codedev.co.id.base.injection.PerService;
import codedev.co.id.base.injection.module.ActivityModule;
import codedev.co.id.base.injection.module.ServiceModule;
import dagger.Component;

@PerService
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, ServiceModule.class})
public interface ServiceComponent {

}
