package codedev.co.id.base.ui.app_location.list;

import android.os.Bundle;
import android.support.annotation.Nullable;

import codedev.co.id.base.ui.base.BaseActivityWithToolbar;

public class SchoolLocationActivity extends BaseActivityWithToolbar {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setupDefaultFragment(new SchoolLocationFragment());
    }
}
