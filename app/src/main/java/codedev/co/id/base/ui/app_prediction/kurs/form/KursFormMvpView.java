package codedev.co.id.base.ui.app_prediction.kurs.form;

import codedev.co.id.base.interfaces.BaseRequestMvpView;

public interface KursFormMvpView extends BaseRequestMvpView {
    void onSucceedSubmitData();
    void onSuceedUpdateData();
}
