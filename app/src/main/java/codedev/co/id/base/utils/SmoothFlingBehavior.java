package codedev.co.id.base.utils;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by fatah.fadhlurrohman (CICIL) on Mon, 06 August 2018
 *
 * This class is used to makes CollapseToolbarLayout become smooth when snapping
 * by enhance the SnapWithNestedScrollView scroll when CollapseToolbarLayout has snap in scrollFlags.
 * Add this class to AppBarLayout behavior in layout_behavior (XML).
 *
 * It was the solution in Bug: CollapsingToolbarLayout sometimes not snappy and sometimes can have artifacts
 * https://issuetracker.google.com/issues/63745189
 *
 */
public class SmoothFlingBehavior extends AppBarLayout.Behavior {

    private boolean isSkipNextStop = true;
    private boolean isNestedStopScroll = true;

    public SmoothFlingBehavior() {
        super();
    }

    public SmoothFlingBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout parent, AppBarLayout child, View directTargetChild, View target, int nestedScrollAxes, int type) {
        if (!isNestedStopScroll) {
            onStopNestedScroll(parent, child, target, type);
            isSkipNextStop = true;
        }

        isNestedStopScroll = false;
        return super.onStartNestedScroll(parent, child, directTargetChild, target, nestedScrollAxes, type);
    }

    @Override
    public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, AppBarLayout abl, View target, int type) {
        if (isSkipNextStop) {
            isSkipNextStop = false;
            return;
        }

        if (isNestedStopScroll) {
            return;
        }

        isNestedStopScroll = true;
        super.onStopNestedScroll(coordinatorLayout, abl, target, ViewCompat.TYPE_TOUCH);
    }
}
