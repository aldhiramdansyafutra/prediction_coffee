package codedev.co.id.base.ui.app_quiz.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import javax.inject.Inject;

import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.constant.FragmentIndex;
import codedev.co.id.base.interfaces.DialogButtonListener;
import codedev.co.id.base.ui.about.AboutActivity;
import codedev.co.id.base.ui.app_quiz.quiz.QuizActivity;
import codedev.co.id.base.ui.app_quiz.quiz.QuizPresenter;
import codedev.co.id.base.ui.app_quiz.score.ScoreActivity;
import codedev.co.id.base.ui.base.BaseActivity;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.DialogFactory;
import codedev.co.id.base.utils.Extras;

public class HomeFragment extends BaseFragment {

    @Inject
    HomePresenter mPresenter;

    @Override
    protected void attachViewForPresenter() {

    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_app_quiz_home;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @OnClick({R.id.btn_quiz_level_1, R.id.btn_quiz_level_2, R.id.btn_quiz_level_3, R.id.btn_about, R.id.btn_score})
    public void handleOnClick(View view) {
        switch (view.getId()) {
            case R.id.btn_quiz_level_1:
                check(QuizPresenter.QuizLevelType.EASY, 60);
                break;
            case R.id.btn_quiz_level_2:
                check(QuizPresenter.QuizLevelType.MEDIUM, 70);
                break;
            case R.id.btn_quiz_level_3:
                check(QuizPresenter.QuizLevelType.HARD, 80);
                break;
            case R.id.btn_about:
                Bundle bundle = new Bundle();
                bundle.putString(Extras.ABOUT_TITLE, "Kuis Cerdas Cermat");
                bundle.putString(Extras.ABOUT_VERSION, "1.0");
                bundle.putString(Extras.ABOUT_LAST_UPDATE, "Mei 2019");
                bundle.putString(Extras.ABOUT_DESCRIPTION, getString(R.string.quiz_about));
                BaseActivity.navigateActivity(getActivity(), bundle, R.string.titla_about, AboutActivity.class);
                animationOpenActivity();
                break;
            case R.id.btn_score:
                Bundle bundleScore = new Bundle();
                bundleScore.putInt(Extras.TITLE_ID, R.string.title_quiz_home);
                startActivity(BaseActivity.getStartIntent(getActivity(), bundleScore, ScoreActivity.class));
                animationOpenActivity();
                break;
        }
    }

    private void goToQuiz(QuizPresenter.QuizLevelType levelType) {
        Bundle bundle = new Bundle();
        bundle.putInt(Extras.TITLE_ID, R.string.title_quiz);
        bundle.putInt(Extras.FRAGMENT_INDEX, FragmentIndex.FRAGMENT_QUIZ);
        bundle.putString(Extras.QUIZ_TITLE_BAR, getString(R.string.title_quiz_ongoing, "1"));
        bundle.putInt(Extras.QUIZ_QUESTION_INDEX, 0);
        bundle.putInt(Extras.QUIZ_CORRECT_ANSWER, 0);
        bundle.putSerializable(Extras.QUIZ_LEVEL, levelType);
        startActivity(BaseActivity.getStartIntent(getActivity(), bundle, QuizActivity.class));
        animationOpenActivity();
    }

    private void check(final QuizPresenter.QuizLevelType levelType, int minScore) {
        if (mPresenter.isQuizAvailable(levelType)) {
            DialogFactory.createDialog(getActivity(), getString(R.string.title_quiz_home), getString(R.string.message_start_quiz, String.valueOf(minScore)), "MULAI", "",
                    new DialogButtonListener() {
                        @Override
                        public void onPositiveDialogButtonListener() {
                            goToQuiz(levelType);
                        }

                        @Override
                        public void onNegativeDialogButtonListener() {

                        }
                    }).show();
        } else {
            DialogFactory.createDialog(getActivity(), getString(R.string.title_quiz_home), getString(R.string.message_rejected_quiz), "", "OKAY", null).show();
        }
    }
}
