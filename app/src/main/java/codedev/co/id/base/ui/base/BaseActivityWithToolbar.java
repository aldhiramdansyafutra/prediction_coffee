package codedev.co.id.base.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import codedev.co.id.base.R;
import codedev.co.id.base.utils.Extras;

public abstract class BaseActivityWithToolbar extends BaseActivity {

    protected int fragmentIndex = -1;

    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;
    @Nullable
    @BindView(R.id.image_logo)
    protected ImageView mImageLogo;
    @Nullable
    @BindView(R.id.text_tkb90)
    protected TextView mTextTkb90;

    @Override
    protected int getContentView() {
        return R.layout.activity_common_with_toolbar;
    }

    @Override
    protected int getTitleBar() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            return getIntent().getExtras().getInt(Extras.TITLE_ID, R.string.default_title_toolbar);
        } else {
            return R.string.default_title_toolbar;
        }
    }

    @Override
    protected int getToolbarColorId() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            return getIntent().getExtras().getInt(Extras.TOOLBAR_COLOR_ID, R.color.colorCicilGreen);
        } else {
            return R.color.colorCicilGreen;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mToolbar != null) {
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);

            int titleBarId = getTitleBar();
            if (titleBarId != 0) {
                String title = getString(getTitleBar());
                if (getIntent().getExtras().containsKey(Extras.QUIZ_TITLE_BAR)) {
                    title = getIntent().getExtras().getString(Extras.QUIZ_TITLE_BAR);
                }
                mToolbar.setTitle(title);
                mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
            }

            int toolbarColorId = getToolbarColorId();
            if (toolbarColorId != 0) {
                mToolbar.setBackgroundColor(getResources().getColor(toolbarColorId));
            }

            ActionBar actionBar = getSupportActionBar();

            if (actionBar != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_left);
            }
        }
        mTextHeaderNoConnection = ButterKnife.findById(this, R.id.text_header_no_connection);
    }
}
