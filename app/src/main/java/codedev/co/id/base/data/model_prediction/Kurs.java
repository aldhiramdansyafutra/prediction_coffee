package codedev.co.id.base.data.model_prediction;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import codedev.co.id.base.data.BaseResponse;

public class Kurs extends BaseResponse implements Parcelable {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("kurs_id")
    @Expose
    public String kursId;
    @SerializedName("nominal")
    @Expose
    public Double nominal;
    @SerializedName("month")
    @Expose
    public int month;
    @SerializedName("year")
    @Expose
    public String year;

    public Kurs() {

    }

    protected Kurs(Parcel in) {
        id = in.readInt();
        kursId = in.readString();
        nominal = in.readDouble();
        month = in.readInt();
        year = in.readString();
    }

    public static final Creator<Kurs> CREATOR = new Creator<Kurs>() {
        @Override
        public Kurs createFromParcel(Parcel in) {
            return new Kurs(in);
        }

        @Override
        public Kurs[] newArray(int size) {
            return new Kurs[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(kursId);
        parcel.writeDouble(nominal);
        parcel.writeInt(month);
        parcel.writeString(year);
    }

    public static class Result {
        @SerializedName("body")
        @Expose
        public ArrayList<Kurs> products;

        public Result() {

        }
    }
}
