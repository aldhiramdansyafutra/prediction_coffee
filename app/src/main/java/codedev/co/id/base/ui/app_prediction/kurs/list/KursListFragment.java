package codedev.co.id.base.ui.app_prediction.kurs.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.data.model_prediction.Kurs;
import codedev.co.id.base.ui.app_prediction.kurs.form.KursFormActivity;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.Extras;

public class KursListFragment extends BaseFragment implements KursMvpView, KursAdapter.OnItemClickListener {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerProduct;
    @BindView(R.id.et_search)
    TextInputEditText etSearch;

    @Inject
    KursPresenter mPresenter;

    KursAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
        mAdapter = new KursAdapter();
        mAdapter.setOnItemClickListener(this);
    }

    @Override
    protected void attachViewForPresenter() {
        mPresenter.attachView(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_prediction_kurs_list;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        etSearch.clearFocus();
        mRecyclerProduct.requestFocus();
        mRecyclerProduct.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecyclerProduct.setHasFixedSize(true);
        mRecyclerProduct.setAdapter(mAdapter);
        mPresenter.getData();
    }

    @OnClick(R.id.fab_add_product)
    void handleNewProduct(View view) {
        goToDetail(null, R.string.kurs_new);
    }

    private void goToDetail(Kurs kurs, int title) {
        Intent intent = new Intent(getActivity(), KursFormActivity.class);
        intent.putExtra(Extras.TITLE_ID, title);
        intent.putExtra(Extras.KURS_DETAIL, kurs);
        startActivity(intent);
    }

    @Override
    public void onItemClick(Kurs item, int pos) {
        goToDetail(item, R.string.kurs_detail);
    }

    @Override
    public void onItemRemove(Kurs item, int pos) {
        mAdapter.removeItem(item);
    }

    @Override
    public void onSucceed(ArrayList<Kurs> results) {
        dissmissLoading();
        mAdapter.addItems(results);
    }

    @Override
    public void showProgressBarLoading() {
        showLoading();
    }

    @Override
    public void onFailedRequest() {
        dissmissLoading();
    }
}
