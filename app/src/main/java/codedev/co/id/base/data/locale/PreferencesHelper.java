package codedev.co.id.base.data.locale;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import codedev.co.id.base.injection.ApplicationContext;


/**
 * Created by franky on 11/12/16.
 */

public class PreferencesHelper {
    public static final String PREF_FILE_NAME = "pref_file_name";

    // Quiz helper
    public static final String PREF_QUIZ_LEVEL_EASY = "pref_quiz_level_easy";
    public static final String PREF_QUIZ_LEVEL_MEDIUM = "pref_quiz_level_medium";
    public static final String PREF_QUIZ_LEVEL_HARD = "pref_quiz_level_hard";
    public static final String PREF_POINT_LEVEL_EASY = "pref_quiz_level_easy";
    public static final String PREF_POINT_LEVEL_MEDIUM = "pref_quiz_level_medium";
    public static final String PREF_POINT_LEVEL_HARD = "pref_quiz_level_hard";

    private SharedPreferences mPref;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void clear() {
        mPref.edit().clear().apply();
    }

    public void putString(String key, String value, String def) {
        mPref.edit().putString(key, value == null ? def : value).apply();
    }

    public void putInt(String key, int value) {
        mPref.edit().putInt(key, value).apply();
    }

    public void putBooelan(String key, boolean value) {
        mPref.edit().putBoolean(key, value).apply();
    }

    public void putFloat(String key, float value) {
        mPref.edit().putFloat(key, value).apply();
    }

    // Preference get helper

    public String getString(String key, String defValue) {
        return mPref.getString(key, defValue);
    }

    public int getInt(String key, int defValue) {
        return mPref.getInt(key, defValue);
    }

    public boolean getBoolean(String key, boolean defValue) {
        return mPref.getBoolean(key, defValue);
    }
}
