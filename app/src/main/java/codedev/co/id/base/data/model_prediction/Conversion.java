package codedev.co.id.base.data.model_prediction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Conversion {

    @SerializedName("code")
    @Expose
    public int code;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("variable")
    @Expose
    public ConversionVariable variable;
    @SerializedName("result")
    @Expose
    public ArrayList<Product> products;
    @SerializedName("count")
    @Expose
    public int count;

    public Conversion() {
        variable = new ConversionVariable();
        products = new ArrayList<>();
    }

}
