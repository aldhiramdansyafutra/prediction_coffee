package codedev.co.id.base.ui.app_prediction.conversion.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import codedev.co.id.base.R;
import codedev.co.id.base.data.model_prediction.Product;

public class ConversionProductAdapter extends RecyclerView.Adapter<ConversionProductAdapter.ConversionProductViewHolder> {

    ArrayList<Product> products;

    public ConversionProductAdapter() {
        products = new ArrayList<>();
    }

    public void addItems(ArrayList<Product> results) {
        if (results != null && results.size() > 0) {
            products.clear();
            products.addAll(results);
            notifyDataSetChanged();
        }
    }

    public Product getAdapterPosition(int position) {
        return products != null && products.size() > 0 ? products.get(position) : new Product();
    }

    @NonNull
    @Override
    public ConversionProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_prediction_product, viewGroup, false);
        return new ConversionProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConversionProductViewHolder holder, int i) {
        if (products.size() > 0) {
            Product product = getAdapterPosition(i);
            holder.tvId.setText(product.productId);
            holder.tvName.setText(product.name);
            holder.tvPrice.setText(String.valueOf(product.price));
            holder.tvSold.setText(String.valueOf(product.soldItem));
            holder.tvMonth.setText(String.valueOf(product.month));
        }
    }

    @Override
    public int getItemCount() {
        return products != null ? products.size() : 0;
    }

    public class ConversionProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_id)
        TextView tvId;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_sold)
        TextView tvSold;
        @BindView(R.id.tv_month)
        TextView tvMonth;

        public ConversionProductViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
