package codedev.co.id.base.data.model_prediction;

import com.google.gson.annotations.SerializedName;

public class GenerateId extends BaseResponse {

    @SerializedName("id")
    public String generatedId;

    public GenerateId() {
    }
}
