package codedev.co.id.base.ui.app_prediction.product.form;

import codedev.co.id.base.data.model_prediction.Product;
import codedev.co.id.base.interfaces.BaseRequestMvpView;

public interface ProductFormMvpView extends BaseRequestMvpView {

    void onSucceedCreateProduct();
    void onSucceedUpdateProduct();
    void onSucceedGetProduct(Product product);
    void onSucceedGenerateId();
}
