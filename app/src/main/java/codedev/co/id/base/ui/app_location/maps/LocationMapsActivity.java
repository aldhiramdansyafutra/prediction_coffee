package codedev.co.id.base.ui.app_location.maps;

import android.os.Bundle;

import codedev.co.id.base.ui.base.BaseActivityWithToolbar;

public class LocationMapsActivity extends BaseActivityWithToolbar {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDefaultFragment(new LocationMapsFragment());
    }
}
