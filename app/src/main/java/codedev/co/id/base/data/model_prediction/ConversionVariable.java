package codedev.co.id.base.data.model_prediction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConversionVariable {

    @SerializedName("x")
    @Expose
    public Double x;
    @SerializedName("y")
    @Expose
    public Double y;
    @SerializedName("x2")
    @Expose
    public Double x2;
    @SerializedName("y2")
    @Expose
    public Double y2;
    @SerializedName("x_y")
    @Expose
    public Double xY;

    public ConversionVariable() {
    }
}
