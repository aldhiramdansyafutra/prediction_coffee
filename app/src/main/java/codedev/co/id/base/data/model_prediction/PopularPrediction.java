package codedev.co.id.base.data.model_prediction;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PopularPrediction implements Parcelable {

    @SerializedName("product_name")
    @Expose
    public String productName;

    @SerializedName("sold_item_total")
    @Expose
    public String soldItemTotal;

    public PopularPrediction() {

    }

    protected PopularPrediction(Parcel in) {
        productName = in.readString();
        soldItemTotal = in.readString();
    }

    public static final Creator<PopularPrediction> CREATOR = new Creator<PopularPrediction>() {
        @Override
        public PopularPrediction createFromParcel(Parcel in) {
            return new PopularPrediction(in);
        }

        @Override
        public PopularPrediction[] newArray(int size) {
            return new PopularPrediction[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(productName);
        parcel.writeString(soldItemTotal);
    }

    public static class Result extends BaseResponse {

        @SerializedName("body")
        @Expose
        public ArrayList<PopularPrediction> results;

        public Result() {
        }
    }
}
