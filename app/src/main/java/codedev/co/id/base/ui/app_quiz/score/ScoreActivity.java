package codedev.co.id.base.ui.app_quiz.score;

import android.os.Bundle;
import android.support.annotation.Nullable;

import codedev.co.id.base.ui.base.BaseActivityWithToolbar;

public class ScoreActivity extends BaseActivityWithToolbar {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDefaultFragment(new ScoreFragment());
    }
}
