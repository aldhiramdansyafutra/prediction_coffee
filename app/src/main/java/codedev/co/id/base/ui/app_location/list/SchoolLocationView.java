package codedev.co.id.base.ui.app_location.list;

import codedev.co.id.base.interfaces.BaseView;

public interface SchoolLocationView extends BaseView {
    void onSucceedGetLocation();
}
