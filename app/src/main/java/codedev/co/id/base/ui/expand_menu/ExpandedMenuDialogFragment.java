package codedev.co.id.base.ui.expand_menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.ui.about.AboutActivity;
import codedev.co.id.base.ui.base.BaseActivity;
import codedev.co.id.base.ui.base.BaseDialogFragment;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.Extras;

public class ExpandedMenuDialogFragment extends BaseDialogFragment {
    private CustomTabsIntent customTabsIntent;
    private CustomTabsIntent.Builder mCustomTabIntentBuilder;

    public static ExpandedMenuDialogFragment newInstance(Bundle bundle) {
        return (ExpandedMenuDialogFragment) BaseFragment.newInstanceFragment(bundle, new ExpandedMenuDialogFragment());
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_fragment_action_bar;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Dialog_NoActionBar);
        mCustomTabIntentBuilder = new CustomTabsIntent.Builder();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().setCanceledOnTouchOutside(true);
        customTabsIntent = mCustomTabIntentBuilder.build();
        try {
            customTabsIntent.intent.setPackage("com.android.chrome");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setGravity(Gravity.TOP);
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.getAttributes().windowAnimations = R.style.ActionBarAnimation;
            window.setBackgroundDrawableResource(android.R.color.transparent);
        }
    }

    @OnClick({R.id.tv_about})
    public void onClickListener(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.tv_about:
                bundle.putString(Extras.ABOUT_TITLE, "Pencarian Lokasi Taman Kanak-Kanak");
                bundle.putString(Extras.ABOUT_VERSION, "1.0");
                bundle.putString(Extras.ABOUT_LAST_UPDATE, "Mei 2019");
                bundle.putString(Extras.ABOUT_DESCRIPTION, getString(R.string.location_about_description));
                BaseActivity.navigateActivity(getActivity(), bundle, R.string.titla_about, AboutActivity.class);
                if (getActivity() != null && getActivity() instanceof BaseActivity)
                    ((BaseActivity) getActivity()).animationOpenActivity();
                break;
        }
        dismiss();
    }
}
