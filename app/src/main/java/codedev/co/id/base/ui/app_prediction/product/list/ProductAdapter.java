package codedev.co.id.base.ui.app_prediction.product.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import codedev.co.id.base.R;
import codedev.co.id.base.data.model_prediction.Product;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private ArrayList<Product> listProduct;
    private OnItemClickListener mOnItemClickListener;

    public ProductAdapter() {
        listProduct = new ArrayList<>();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public void addItems(ArrayList<Product> products) {
        if (products != null && products.size() > 0) {
            listProduct.clear();
            listProduct.addAll(products);
            notifyDataSetChanged();
        }
    }

    public void removeItem(Product product) {
        listProduct.remove(product);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_prediction_product_list, viewGroup, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, final int i) {
        if (listProduct != null && listProduct.size() > 0) {
            final Product product = getItemAdapter(i);
            holder.tvInitial.setText(product.name.subSequence(0, 2));
            holder.tvName.setText(product.name);
            holder.tvPrice.setText(String.valueOf(product.price));
            holder.tvSold.setText(String.valueOf(product.soldItem));
            holder.tvDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onItemRemove(product, i);
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onItemClick(product, i);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listProduct != null ? listProduct.size() : 0;
    }

    public Product getItemAdapter(int position) {
        return listProduct.get(position);
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        public TextView tvInitial;
        public TextView tvName;
        public TextView tvPrice;
        public TextView tvSold;
        public ImageView tvDelete;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            tvInitial = itemView.findViewById(R.id.tv_initial);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPrice = itemView.findViewById(R.id.tv_price);
            tvSold = itemView.findViewById(R.id.tv_sold);
            tvDelete = itemView.findViewById(R.id.ic_delete);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Product product, int pos);

        void onItemRemove(Product product, int pos);
    }
}
