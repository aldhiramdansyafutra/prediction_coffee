package codedev.co.id.base.ui.app_prediction.conversion;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import codedev.co.id.base.R;
import codedev.co.id.base.data.model_prediction.Conversion;
import codedev.co.id.base.ui.app_prediction.conversion.adapter.ConversionProductAdapter;
import codedev.co.id.base.ui.base.BaseFragment;
import codedev.co.id.base.utils.StringHelper;

public class ConversionFragment extends BaseFragment implements ConversionMvpView {

    @BindView(R.id.til_product_id)
    TextInputLayout tilProductId;
    @BindView(R.id.til_year)
    TextInputLayout tilYear;

    @BindView(R.id.recycler_product)
    RecyclerView recyclerProduct;
    @BindView(R.id.recycler_prediction)
    RecyclerView recyclerPrediction;

    @Inject
    ConversionPresenter mPresenter;

    ConversionProductAdapter mProductAdapter;
    ConversionProductAdapter mPredictionAdapter;

    @Override
    protected void attachViewForPresenter() {
        mPresenter.attachView(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_prediction_conversion;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
        initInstance();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override
    public void onSucceed(Conversion result) {
        mProductAdapter.addItems(result.products);
        mPresenter.calculatePrediction();
    }

    @Override
    public void onPredictionSucceed() {
        dissmissLoading();
        mPredictionAdapter.addItems(mPresenter.mPredictionResult);
    }

    @Override
    public void showProgressBarLoading() {
        showLoading();
    }

    @Override
    public void onFailedRequest() {
        dissmissLoading();
    }

    @OnClick(R.id.btn_conversion)
    void handleOnClick() {
        String productId = tilProductId.getEditText().getText().toString();
        String year = tilYear.getEditText().getText().toString();
        if (StringHelper.isNotEmpty(productId) || StringHelper.isNotEmpty(year)) {
            mPresenter.getDataConversion(productId, year);
        } else {
            // TODO validation
        }
    }

    private void initInstance() {
        mProductAdapter = new ConversionProductAdapter();
        mPredictionAdapter = new ConversionProductAdapter();
    }

    private void initView() {
        recyclerProduct.setHasFixedSize(true);
        recyclerProduct.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerProduct.setAdapter(mProductAdapter);

        recyclerPrediction.setHasFixedSize(true);
        recyclerPrediction.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerPrediction.setAdapter(mPredictionAdapter);
    }
}
