package codedev.co.id.base.ui.app_prediction.product.form;

import android.os.Bundle;
import android.support.annotation.Nullable;

import codedev.co.id.base.ui.base.BaseActivityWithToolbar;

public class ProductFormActivity extends BaseActivityWithToolbar {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupFragment(getIntent().getExtras());
    }

    private void setupFragment(Bundle bundle) {
        ProductFormFragment fragment = new ProductFormFragment();
        fragment.setArguments(bundle);
        setupDefaultFragment(fragment);
    }
}
