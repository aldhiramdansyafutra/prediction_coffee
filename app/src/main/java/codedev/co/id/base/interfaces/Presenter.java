package codedev.co.id.base.interfaces;

public interface Presenter<V extends MvpView> {

    void attachView(V view);
    void detachView();
}
