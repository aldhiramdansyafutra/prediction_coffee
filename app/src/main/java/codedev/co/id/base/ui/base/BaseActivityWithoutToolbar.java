package codedev.co.id.base.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import codedev.co.id.base.R;
import codedev.co.id.base.utils.Extras;

public abstract class BaseActivityWithoutToolbar extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.activity_common_without_toolbar;
    }

    @Override
    protected int getTitleBar() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            return getIntent().getExtras().getInt(Extras.TITLE_ID, R.string.default_title_toolbar);
        } else {
            return R.string.default_title_toolbar;
        }
    }

    @Override
    protected int getToolbarColorId() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            return getIntent().getExtras().getInt(Extras.TOOLBAR_COLOR_ID, R.color.colorCicilGreen);
        } else {
            return R.color.colorCicilGreen;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
