package codedev.co.id.base.ui.app_prediction.home;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import javax.inject.Inject;

import codedev.co.id.base.data.DataManager;
import codedev.co.id.base.data.model_prediction.PopularPrediction;
import codedev.co.id.base.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter extends BasePresenter<HomeMvpView> {

    public static final int DATA_SHOW_LIMIT = 5;

    ArrayList<IBarDataSet> mDataSet;
    ArrayList<String> mAxisValues;

    @Inject
    public HomePresenter(DataManager dataManager) {
        mDataManager = dataManager;
        mDataSet = new ArrayList<>();
        mAxisValues = new ArrayList<>();
    }

    public void getPopularProduct(String year) {
        mMvpView.showProgressBarLoading();
        mDataManager.getPopularPrediction(year, String.valueOf(DATA_SHOW_LIMIT)).enqueue(new Callback<PopularPrediction.Result>() {
            @Override
            public void onResponse(Call<PopularPrediction.Result> call, Response<PopularPrediction.Result> response) {
                if (response.body() != null) {
                    parseChartData(response.body().results);
                    mMvpView.onSucceedGetProduct(response.body());
                } else {
                    mMvpView.onFailedRequest();
                }
            }

            @Override
            public void onFailure(Call<PopularPrediction.Result> call, Throwable t) {
                mMvpView.onFailedRequest();
            }
        });
    }

    public BarData getBarData() {
        return new BarData(mAxisValues, mDataSet);
    }

    private void parseChartData(ArrayList<PopularPrediction> datas) {
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        mDataSet.clear();
        mAxisValues.clear();
        if (datas != null) {
            for (int i = 0; i < datas.size(); i++) {
                valueSet1.add(new BarEntry(Float.parseFloat(String.valueOf(datas.get(i).soldItemTotal)), i));
                mAxisValues.add(datas.get(i).productName);
            }
        }

        if (valueSet1.size() > 0) {
            BarDataSet barDataSet1 = new BarDataSet(valueSet1, "");
            barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
            mDataSet.add(barDataSet1);
        }
    }
}
