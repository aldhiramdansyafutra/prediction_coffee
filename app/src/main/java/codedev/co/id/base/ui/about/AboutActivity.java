package codedev.co.id.base.ui.about;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;

import codedev.co.id.base.ui.base.BaseActivityWithToolbar;
import codedev.co.id.base.ui.base.BaseFragment;

public class AboutActivity extends BaseActivityWithToolbar {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDefaultFragment(BaseFragment.newInstanceFragment(getIntent().getExtras(), new AboutFragment()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        return true;
    }
}
