package codedev.co.id.base.ui.app_prediction.kurs.list;

import java.util.ArrayList;

import codedev.co.id.base.data.model_prediction.Kurs;
import codedev.co.id.base.data.model_prediction.Product;
import codedev.co.id.base.interfaces.BaseRequestMvpView;
import codedev.co.id.base.interfaces.MvpView;

public interface KursMvpView extends BaseRequestMvpView {
    void onSucceed(ArrayList<Kurs> results);
}
