package codedev.co.id.base.interfaces;

import codedev.co.id.base.constant.Constant;

public interface BaseView extends MvpView {
    void showLoading(Constant.LoadingType type);
    void onError(String errMessage);
}
