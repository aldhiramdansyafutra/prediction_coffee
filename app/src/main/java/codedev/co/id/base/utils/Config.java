package codedev.co.id.base.utils;

public class Config {

    public static final int MAX_SIZE_PHOTO = 1080;
    public static final int MAX_SIZE_CONTAINER_PHOTO_DP = 200;
    public static final String INDONESIA_PHONE_CODE = "+62";
    public static final String EMAIL_CONTACT_US = "contactus@cicil.co.id";
    public static final String NORMAL_DATE_FORMAT = "dd MMMM yyyy";
    public static final String NORMAL_DATE_WITH_TIME_FORMAT = "dd MMMM yyyy HH:mm";
    public static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd";
    public static final String PAYMENT_PURPOSE_DP = "DP";
    public static final String PAYMENT_PURPOSE_INSTALLMENT = "INSTALLMENT";
    public static final String BANK_NAME = "PT Cicil Solusi Mitra Teknologi";
    public static final String BANK_METHOD_MANUAL = "manual_transfer";
    public static final String BANK_METHOD_VA = "virtual_account";
    public static final int ADDING_DAY_FOR_DP = 5;
    public static final String STATUS_DELETED = "deleted";
    public static final String STATUS_PENDING = "pending";
    public static final String STATUS_IN_REVIEW = "in_review";
    public static final String STATUS_ON_HOLD = "on_hold";
    public static final String STATUS_REJECTED = "rejected";
    public static final String STATUS_APPROVED = "approved";
    public static final String STATUS_BLACKLIST = "blacklist";
    public static int googleAPIClientIndex = 0;
    public static final String AXA_WEB_URL = "http://blog.cicil.co.id/index.php/2018/08/27/asuransi-cicilan-uang-kuliah/";

    public static final String CICIL_REFERRAL_CODE = "cicil_referral_code";
    public static final String URL_SURBO = "https://bot.surbo.io/web-bot/5bd181cb807c0a7d3c705909";
}
