package codedev.co.id.base.data.model_prediction;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Product implements Parcelable {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("price")
    @Expose
    public int price;
    @SerializedName("sold")
    @Expose
    public int soldItem;
    @SerializedName("month")
    @Expose
    public int month;
    @SerializedName("year")
    @Expose
    public String year;

    public Product() {

    }

    protected Product(Parcel in) {
        id = in.readInt();
        productId = in.readString();
        name = in.readString();
        price = in.readInt();
        soldItem = in.readInt();
        month = in.readInt();
        year = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(productId);
        parcel.writeString(name);
        parcel.writeInt(price);
        parcel.writeInt(soldItem);
        parcel.writeInt(month);
        parcel.writeString(year);
    }

    public static class Result extends BaseResponse {
        @SerializedName("body")
        @Expose
        public ArrayList<Product> products;

        public Result() {

        }
    }
}
