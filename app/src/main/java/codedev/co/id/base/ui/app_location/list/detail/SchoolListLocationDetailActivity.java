package codedev.co.id.base.ui.app_location.list.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;

import codedev.co.id.base.ui.base.BaseActivityWithCollapssingImageToolbar;
import codedev.co.id.base.ui.base.BaseFragment;

public class SchoolListLocationDetailActivity extends BaseActivityWithCollapssingImageToolbar {

    @Override
    protected int getImageToolbar() {
        return 0;
    }

    @Override
    protected int getCollapsableToolbarColor() {
        return 0;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = new Bundle();
        if (getIntent().getExtras() != null) {
            bundle = getIntent().getExtras();
        }
        setupDefaultFragment(BaseFragment.newInstanceFragment(bundle, new SchoolListLocationDetailFragment()));
    }
}
