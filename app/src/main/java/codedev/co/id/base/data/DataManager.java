package codedev.co.id.base.data;

import com.google.gson.JsonObject;

import javax.inject.Inject;

import codedev.co.id.base.data.model_prediction.BaseResponse;
import codedev.co.id.base.data.model_prediction.Conversion;
import codedev.co.id.base.data.model_prediction.ConversionRequest;
import codedev.co.id.base.data.model_prediction.GenerateId;
import codedev.co.id.base.data.model_prediction.Kurs;
import codedev.co.id.base.data.model_prediction.PopularPrediction;
import codedev.co.id.base.data.model_prediction.Product;
import codedev.co.id.base.data.network.ApiServices;
import codedev.co.id.base.utils.StringHelper;
import retrofit2.Call;

public class DataManager {

    ApiServices mApiServices;

    @Inject
    public DataManager(ApiServices apiServices) {
        mApiServices = apiServices;
    }

    public Call<Product.Result> getAllProduct() {
        return mApiServices.getAllProduct();
    }

    public Call<BaseResponse> createProduct(Product product) {
        return mApiServices.createProduct(product);
    }

    public Call<BaseResponse> updateProduct(Product product) {
        return mApiServices.updateProduct(product);
    }

    public Call<GenerateId> generateProductId() {
        return mApiServices.generateProductId();
    }

    public Call<BaseResponse> deleteProduct(int id) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("product_id", id);
        return mApiServices.deleteProduct(StringHelper.convertToRequestBody(jsonObject));
    }

    public Call<Product.Result> getProduct(String productId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("product_id", productId);
        return mApiServices.getProduct(StringHelper.convertToRequestBody(jsonObject));
    }

    public Call<PopularPrediction.Result> getPopularPrediction(String year, String limit) {
        return mApiServices.getPopularPrediction(year, limit);
    }

    public Call<Conversion> getConversion(ConversionRequest request) {
        return mApiServices.getConversion(request);
    }

    public Call<Kurs.Result> getKurs() {
        return mApiServices.getKurs();
    }
}
