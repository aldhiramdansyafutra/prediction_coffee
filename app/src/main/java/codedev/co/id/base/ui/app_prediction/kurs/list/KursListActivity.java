package codedev.co.id.base.ui.app_prediction.kurs.list;

import android.os.Bundle;
import android.support.annotation.Nullable;

import codedev.co.id.base.ui.base.BaseActivityWithToolbar;

public class KursListActivity extends BaseActivityWithToolbar {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDefaultFragment(new KursListFragment());
    }
}
