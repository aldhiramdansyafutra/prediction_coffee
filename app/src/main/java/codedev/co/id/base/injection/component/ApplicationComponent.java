package codedev.co.id.base.injection.component;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.common.GoogleApiAvailability;

import javax.inject.Singleton;

import codedev.co.id.base.BaseApplication;
import codedev.co.id.base.data.DataManager;
import codedev.co.id.base.injection.ApplicationContext;
import codedev.co.id.base.injection.module.ApplicationModule;
import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext
    Context context();

    Application application();

    GoogleApiAvailability googleApiAvailability();

    DataManager dataManager();

    void inject(BaseApplication baseAppilication);
}
