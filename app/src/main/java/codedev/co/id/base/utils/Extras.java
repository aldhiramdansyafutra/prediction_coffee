package codedev.co.id.base.utils;

public class Extras {
    public static final String IMAGE_URL = "image_url";
    public static final String TITLE_ID = "titleId";
    public static final String BANNER = "banner";
    public static final String TOOLBAR_COLOR_ID = "toolbarColorId";
    public static final String IS_FROM_TUITION_SIMULATION = "isFromTuitionSimulation";
    public static final String FRAGMENT_INDEX = "fragment_index";

    public static final String BANNER_TYPE_ICON = "banner_type_icon";

    // About
    public static final String ABOUT_TITLE = "about_title";
    public static final String ABOUT_VERSION = "about_version";
    public static final String ABOUT_LAST_UPDATE = "about_last_update";
    public static final String ABOUT_DESCRIPTION = "about_description";

    // Location Detail
    public static final String DETAIL_TITLE = "detail_title";
    public static final String DETAIL_DESC = "detail_description";
    public static final String DETAIL_PHONE = "detail_phone";
    public static final String DETAIL_ADDRESS = "detail_address";
    public static final String DETAIL_LATITUDE = "detail_latitude";
    public static final String DETAIL_LONGITUDE = "detail_longitude";
    public static final String DETAIL_LOCATION = "detail_location";

    // Quiz
    public static final String QUIZ_CORRECT_ANSWER = "quiz_correct_answer";
    public static final String QUIZ_LEVEL = "quiz_level";
    public static final String QUIZ_POINT_RESULT = "quiz_point_result";
    public static final String QUIZ_QUESTION_INDEX = "quiz_index";
    public static final String QUIZ_TITLE_BAR = "quiz_title_bar";
    public static final String QUIZ_RESULT = "quiz_result";

    // Prediction
    public static final String PRODUCT_DETAIL = "product_detail";
    public static final String KURS_DETAIL = "kurs_detail";

}
